#!/usr/bin/env python

import rospy
import rospkg

import sys
import os
import errno
import pickle
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import copy
import numpy as np

from hamp_masters_thesis_tests.metrics_plotter import MetricsPlotter

from panda_rrtconnect_test3 import parameter_sweep


def read_data_from_file(dir_path, filename):
    full_dir_path = dir_path
    full_path = full_dir_path + filename
    
    with open(full_path, 'rb') as f:
        data = pickle.load(f)

    return data

if __name__ == "__main__":
    rospy.init_node("generate_plots_from_files")
    rospack = rospkg.RosPack()

    myargv = rospy.myargv(argv=sys.argv)

    TEST_NAME = myargv[1]
    PARAMETER = myargv[2]

    TEST_PKG_NAME = 'hamp_masters_thesis_tests'
    RELATIVE_PATH_TRAJ_RRTCC = "/rrt/rrtconnect_costmap/"+ TEST_NAME +"/trajectories/"

    rrt_connect_costmap_metrics_dir_path = "/rrt/rrtconnect_costmap/" + TEST_NAME + "/metrics/"

    hamp_masters_thesis_tests_dir = rospack.get_path(TEST_PKG_NAME)

    metrics_plotter = MetricsPlotter(TEST_PKG_NAME)

    avg_path_length = []
    avg_planning_time = []
    integral_cost = []
    mechanical_work = []
    avg_separation_dist = []
    avg_num_nodes = []
    avg_inertia = []
    avg_visibility = []
    for i in range(len(parameter_sweep)):
        avg_path_length.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'path_length_.pkl'))
        avg_planning_time.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'planning_time_.pkl'))
        integral_cost.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'path_integral_cost_.pkl'))
        mechanical_work.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'path_mechanical_work_.pkl'))
        avg_separation_dist.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'avg_separation_dist_.pkl'))
        avg_num_nodes.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'num_nodes_.pkl'))
        avg_inertia.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'avg_inertia_.pkl'))
        avg_visibility.append(read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path + str(parameter_sweep[i]) + "/", 'percent_within_gaze_cone_.pkl'))

    if PARAMETER == "alpha":
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_mw.pdf", r'$ \alpha $', "Mechanical Work", mechanical_work, parameter_sweep, title=r'$ \alpha $ vs. Mechanical Work',color='r')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_ic.pdf", r'$ \alpha $', "Integral Cost", integral_cost, parameter_sweep, title=r'$ \alpha $ vs. Integral Cost', color='g')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_pl.pdf", r'$ \alpha $', "Average Path Length (m)", avg_path_length, parameter_sweep, title=r'$ \alpha $ vs. Avg. Path Length', color='b')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_pt.pdf", r'$ \alpha $', "Average Planning Time (s)", avg_planning_time, parameter_sweep, title=r'$ \alpha $ vs. Avg. Planning Time', color='orange')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_sep_dist.pdf", r'$ \alpha $', "Average Separation Distance (m)", avg_separation_dist, parameter_sweep, title=r'$ \alpha $ vs. Avg. Separation Dist.', color='violet')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_num_nodes.pdf", r'$ \alpha $', "Average Number of Nodes", avg_num_nodes, parameter_sweep, title=r'$ \alpha $ vs. Avg. Number of Nodes', color='magenta')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_inertia.pdf", r'$ \alpha $', "Average Inertia ($kg \cdot m^2$)", avg_inertia, parameter_sweep, title=r'$ \alpha $ vs. Avg. Inertia', color='goldenrod')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "alpha_comparison_avg_visibility.pdf", r'$ \alpha $', "Percent of Path within Gaze Cone", avg_visibility, parameter_sweep, title=r'$ \alpha $ vs. \% of path within eFOV', color='teal')
    elif PARAMETER == "eta":
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_mw.pdf", r'$ \eta $', "Mechanical Work", mechanical_work, parameter_sweep, title=r'$ \eta $ vs. Mechanical Work', color='r')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_ic.pdf", r'$ \eta $', "Integral Cost", integral_cost, parameter_sweep, title=r'$ \eta $ vs. Integral Cost', color='g')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_pl.pdf", r'$ \eta $', "Average Path Length (m)", avg_path_length, parameter_sweep, title=r'$ \eta $ vs. Avg. Path Length', color='b')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_pt.pdf", r'$ \eta $', "Average Planning Time (s)", avg_planning_time, parameter_sweep, title=r'$ \eta $ vs. Avg. Planning Time', color='orange')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_sep_dist.pdf", r'$ \eta $', "Average Separation Distance (m)", avg_separation_dist, parameter_sweep, title=r'$ \eta $ vs. Avg. Separation Dist.', color='violet')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_num_nodes.pdf", r'$ \eta $', "Average Number of Nodes", avg_num_nodes, parameter_sweep, title=r'$ \eta $ vs. Avg. Number of Nodes', color='magenta')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_inertia.pdf", r'$ \eta $', "Average Inertia ($kg \cdot m^2$)", avg_inertia, parameter_sweep, title=r'$ \eta $ vs. Avg. Inertia', color='goldenrod')
        metrics_plotter.plot_parameter_sensitivity('/' + TEST_NAME + '/', "eta_comparison_avg_visibility.pdf", r'$ \eta $', "Percent of Path within Gaze Cone", avg_visibility, parameter_sweep, title=r'$ \eta $ vs. \% of path within eFOV', color='teal')
    else:
        raise Exception("PARAMETER should be one of 'alpha' or 'eta'")
