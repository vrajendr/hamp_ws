#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
import tf
import numpy as np 
from visualization_msgs.msg import Marker

from human_aware_planning.panda_model_info_generation import PandaModelInfoGeneration
from human_aware_planning.human_model_info_generation import HumanModelInfoGeneration

import human_aware_planning.rviz_tools as rviz_tools

from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, Polygon

panda_joint_angles = None
human_joint_angles = None

def panda_joint_states_callback(message):
    global panda_joint_angles
    panda_joint_angles = message.position[2:] #go from panda_joint1 to panda_joint7


def human_joint_states_callback(message):
    global human_joint_angles
    human_joint_angles = message.position[6:]

def make_com_tf(tf_name, com, tf_frame):
    br = tf.TransformBroadcaster()
    br.sendTransform((com[0], com[1], com[2]),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     rospy.Time.now(),
                     tf_name,
                     tf_frame)

def create_sphere(frame_id, position, id, name_space=None):
    sphere = Marker()
    sphere.header.frame_id = frame_id
    sphere.header.stamp = rospy.Time.now()
    # action number 0 is add/modify a marker
    sphere.action = 0
    # type number 2 is sphere markers
    sphere.type = 2
    #sphere.ns = name_space
    sphere.id = id
    sphere.pose.position.x = position[0]
    sphere.pose.position.y = position[1]
    sphere.pose.position.z = position[2]
    sphere.pose.orientation.x = 0.0
    sphere.pose.orientation.y = 0.0
    sphere.pose.orientation.z = 0.0
    sphere.pose.orientation.w = 0.0
    # NOTE: Currently hard-coded sphere properties
    sphere.scale.x = 0.11
    sphere.scale.y = 0.11
    sphere.scale.z = 0.11
    sphere.color.a = 1.0
    sphere.color.r = 0.0
    sphere.color.g = 1.0
    sphere.color.b = 0.0
    return sphere

if __name__ == '__main__':

    rospy.init_node("panda_human_com_tf_pub")
    rospy.Subscriber("joint_states", JointState, panda_joint_states_callback, queue_size=1)
    rospy.Subscriber("human_1/joint_states", JointState, human_joint_states_callback, queue_size=1)

    markers_pub = rospy.Publisher("/model_coms", Marker, queue_size=1)

    r = rospy.Rate(20) # 20hz
    pmig = PandaModelInfoGeneration()   
    hmig = HumanModelInfoGeneration()   

    markers = rviz_tools.RvizMarkers('/panda_link0', 'inertia_text')

    while not rospy.is_shutdown():
        panda_com = pmig.generate_info(np.array(panda_joint_angles)).center_of_mass
        panda_inertia = pmig.generate_info(np.array(panda_joint_angles)).inertia
        human_com = hmig.generate_info(np.array(human_joint_angles)).center_of_mass

        #print(panda_inertia)
        
        # # Publish inertia (highest eigenvalue of inertia tensor) as text under the CoM marker
        # P = Pose(Point(panda_com[0]+0.1,panda_com[1],panda_com[2] + 0.12),Quaternion(0,0,0,1))
        # scale = Vector3(0.075,0.075,0.075)
        # panda_inertia_rounded = round(panda_inertia, 3)
        # inertia_string = "Inertia: " + str(panda_inertia_rounded)
        # markers.publishText(P, inertia_string, 'black', scale) # pose, text, color, scale, lifetime

        #make_com_tf("panda_com", panda_com, "/panda_link0")
        #make_com_tf("human_com", human_com, "/human_1/human/base")
        #print(human_com)
        panda_com_marker = create_sphere("/panda_link0", panda_com, 0)
        #human_com_marker = create_sphere("/human_1/human/base", human_com, 1)
        markers_pub.publish(panda_com_marker)
        #markers_pub.publish(human_com_marker)



        
        r.sleep()

    