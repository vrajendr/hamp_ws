#!/usr/bin/env python

import rospy
import rospkg

import math
import operator
import sys
import os
import errno
import pickle
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import copy
import numpy as np

from hamp_masters_thesis_tests.metrics_plotter import MetricsPlotter

def read_data_from_file(dir_path, filename):
    full_dir_path = dir_path
    full_path = full_dir_path + filename
    
    with open(full_path, 'rb') as f:
        data = pickle.load(f)

    return data


def com_dist_along_traj(planner_traj_com, planner_traj_human_com):
    planner_com_dist = []
    com_dist_list = []
    for i in range(len(planner_traj_com)):
        for j in range(len(planner_traj_com[i])):
            hx = planner_traj_human_com[i][j][0]
            hy = planner_traj_human_com[i][j][1]
            hz = planner_traj_human_com[i][j][2]
            dx_CoM = planner_traj_com[i][j][0] - hx  
            dy_CoM = planner_traj_com[i][j][1] - hy
            dz_CoM = planner_traj_com[i][j][2] - hz
            CoM_dist = math.sqrt(dx_CoM*dx_CoM + dy_CoM*dy_CoM  + dz_CoM *dz_CoM)
            com_dist_list.append(CoM_dist)

        planner_com_dist.append(com_dist_list)
        com_dist_list = []
    
    return planner_com_dist


if __name__ == "__main__":
    rospy.init_node("generate_plots_from_files")
    rospack = rospkg.RosPack()

    myargv = rospy.myargv(argv=sys.argv)

    TEST_NAME = myargv[1]

    TEST_PKG_NAME = 'hamp_masters_thesis_tests'

    RELATIVE_PATH_TRAJ_RRTC = "/rrt/rrtconnect/"+ TEST_NAME +"/trajectories/"
    RELATIVE_PATH_TRAJ_RRTCC = "/rrt/rrtconnect_costmap/"+ TEST_NAME +"/trajectories/"

    rrt_connect_metadata_dir_path = "/rrt/rrtconnect/" + TEST_NAME + "/"
    rrt_connect_metadata_file_name = "rrt_connect_metadata.yaml"
    rrt_connect_costmap_metadata_dir_path = "/rrt/rrtconnect_costmap/" + TEST_NAME + "/"
    rrt_connect_costmap_metadata_file_name = "rrt_connect_costmap_metadata.yaml"

    rrt_connect_costmap_metrics_dir_path = "/rrt/rrtconnect_costmap/" + TEST_NAME + "/metrics/"
    rrt_connect_metrics_dir_path = "/rrt/rrtconnect/" + TEST_NAME + "/metrics/"

    hamp_masters_thesis_tests_dir = rospack.get_path(TEST_PKG_NAME)

    metrics_plotter = MetricsPlotter(TEST_PKG_NAME)

    #copy metrics into short variable names
    rrtc_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'traj_inertias_.pkl')
    rrtcc_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'traj_inertias_.pkl')

    rrtc_traj_com = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'traj_com_.pkl')
    rrtcc_traj_com = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'traj_com_.pkl')

    rrtc_traj_human_com = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'traj_human_com_.pkl')
    rrtcc_traj_human_com = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'traj_human_com_.pkl')

    rrtc_path_com = com_dist_along_traj(rrtc_traj_com, rrtc_traj_human_com)
    rrtcc_path_com = com_dist_along_traj(rrtcc_traj_com, rrtcc_traj_human_com)

    p1_avg_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'avg_inertia_.pkl')
    p2_avg_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'avg_inertia_.pkl')

    p1_avg_traj_com = [(sum(avg_com_list) / len(avg_com_list))  for avg_com_list in rrtc_path_com]
    p2_avg_traj_com = [(sum(avg_com_list) / len(avg_com_list)) for avg_com_list in rrtcc_path_com]

    data_labels = ['Human-Aware RRT-Connect', 'RRT-Connect']
    metrics_plotter.plot_inertia_com_subplots('/' + TEST_NAME + '/', "planner_inertia_com.pdf", data_labels, [p2_avg_traj_inertia,p2_avg_traj_com], [p1_avg_traj_inertia,p1_avg_traj_com], colours=['orange', 'blue'])

    # metrics_plotter.plot_full_trajectory_metric(rrtcc_traj_inertia, '/' + TEST_NAME + '/', "traj_avg_inertia.pdf", labelx="Waypoint Index", labely="Robot Inertia $kg \cdot m^2$", clear=True)
    # metrics_plotter.plot_full_trajectory_metric(rrtc_traj_inertia, '/' + TEST_NAME + '/', "traj_avg_inertia.pdf", labelx="Waypoint Index", labely="Robot Inertia $kg \cdot m^2$", color='r')
    # metrics_plotter.plot_full_trajectory_metric(rrtcc_path_com, '/' + TEST_NAME + '/', "traj_avg_com_dist.pdf", labelx="Waypoint Index", labely="CoM Distance (m)", clear=True)
    # metrics_plotter.plot_full_trajectory_metric(rrtc_path_com, '/' + TEST_NAME + '/', "traj_avg_com_dist.pdf", labelx="Waypoint Index", labely="CoM Distance (m)", color='r')

    data_labels = ['Human-Aware RRT-Connect', 'RRT-Connect']
    metrics_plotter.plot_full_trajectory_metric([rrtcc_traj_inertia, rrtc_traj_inertia], data_labels,'/' + TEST_NAME + '/', "traj_avg_inertia.pdf", labelx="Waypoint Index", labely="Robot Inertia ($kg \cdot m^2$)", title="Robot Inertia Along Path", colors=['orange', 'blue'], clear=True)
    metrics_plotter.plot_full_trajectory_metric([rrtcc_path_com, rrtc_path_com], data_labels,'/' + TEST_NAME + '/', "traj_avg_com.pdf", labelx="Waypoint Index", labely="CoM Distance (m)", title="Human-Robot CoM Distance Along Path", colors=['orange', 'blue'], clear=True)
    
