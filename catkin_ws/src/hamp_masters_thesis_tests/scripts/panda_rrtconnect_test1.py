#!/usr/bin/env python

import os
import sys
sys.setrecursionlimit(10000) #to avoid recursion limit errors
import time
import pinocchio as pin
import numpy as np
import random

import rospkg
import rospy
import moveit_commander
from moveit_msgs.msg import DisplayTrajectory
from moveit_msgs.msg import RobotTrajectory, RobotState
from geometry_msgs.msg import Point, Vector3
from std_msgs.msg import Empty, Header, Float64MultiArray

from hamp_masters_thesis_tests.trajectory_utils import TrajectoryUtils
from hamp_masters_thesis_tests.trajectory_saver import TrajectorySaver
from hamp_masters_thesis_tests.trajectory_metrics_generator import TrajectoryMetricsGenerator
from hamp_masters_thesis_tests.forward_kinematics import ForwardKinematics
from hamp_masters_thesis_tests.time_parametrization import TimeParametrizationClient
from hamp_masters_thesis_tests.joint_trajectory_client import JointTrajectoryClient, PANDA_JOINT_NAMES

from augmented_motion_planner.rrt_planners import *
from augmented_motion_planner.rrt_common import *

import human_aware_planning.panda_configuration_space as CONF
import human_aware_planning.static_workspace_collision_check as STAT_COLL
from human_aware_planning.panda_model_info_generation import PandaModelInfoGeneration
from human_aware_planning.msg import DrawPoints

import human_aware_planning.rviz_tools as rviz_tools

from hamp_masters_thesis_tests.metrics_plotter import MetricsPlotter

import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

#simple scene
from panda_human_configs import *

from geometry_msgs.msg import Pose

#cluttered scene
# from panda_human_configs_ps2 import *

import moveit_commander
#from moveit_python import *

global human_com

TEST_PKG_NAME = 'hamp_masters_thesis_tests'
NODE_NAME = "test_15_cluttered_handover"
RELATIVE_PATH_TRAJ_RRTC = "/rrt/rrtconnect/"+ NODE_NAME +"/trajectories/"
RELATIVE_PATH_TRAJ_RRTCC = "/rrt/rrtconnect_costmap/"+ NODE_NAME +"/trajectories/"
NUM_RUNS = 10
ENABLE_VISUALIZATIONS = True
DISPLAY_TRAJ = False
EXECUTE_TRAJECTORY = True
EXECUTE_TRAJECTORY_WAIT = 1.0
RRTCONNECT_PATH_COLOUR = "blue"
RRTCONNECT_COSTMAP_PATH_COLOUR = "orange"
PATH_WIDTH = 0.01
N_POINTS = 100
CLUTTERED = False
HANDOVER = False

#metadata includes all shared parameters between RRT-Connect and RRT-Connect Costmap + some specific to RRT-Connect Costmap
rrt_connect_metadata = {"W_DIST": 0.4,
                        "W_VIS": 0.3,
                        "W_INERTIA": 1.0,
                        "W_DC": 0.3,
                        "MIN_DIST": 0.1,
                        "MIN_COM_DIST": 0.8,
                        "STEP_SIZE": 0.02,
                        "RANGE": 0.1,
                        "MAX_GAP": 0.02,
                        "MAX_ITERATIONS": 100000,
                        "SMOOTHING_ITERATIONS": 0, #0 means SMOOTHING_ITERATIONS = num of states on trajectory
                        "EE_DIST": False,
                        "NUM_RUNS": NUM_RUNS} 


rrt_connect_metadata_dir_path = "/rrt/rrtconnect/" + NODE_NAME + "/"
rrt_connect_metadata_file_name = "rrt_connect_metadata.yaml"
rrt_connect_costmap_metadata_dir_path = "/rrt/rrtconnect_costmap/" + NODE_NAME + "/"
rrt_connect_costmap_metadata_file_name = "rrt_connect_costmap_metadata.yaml"

rrt_connect_costmap_metrics_dir_path = "/rrt/rrtconnect_costmap/" + NODE_NAME + "/metrics/"
rrt_connect_metrics_dir_path = "/rrt/rrtconnect/" + NODE_NAME + "/metrics/"

def create_display_trajectory(robot_start, traj):
    display_trajectory = DisplayTrajectory()
    display_trajectory.trajectory_start = robot_start
    display_trajectory.trajectory.append(traj)

    return display_trajectory

def draw_ee_traj(traj, colour, rviz_tools_instance, fk_instance):
    rviz_tools_instance.publishPath(fk_instance.convert_traj_to_fk_poses(traj.joint_trajectory), colour, PATH_WIDTH)


def human_com_callback(msg):
    global human_com 
    human_com = [msg.x, msg.y, msg.z]

def spinOnce():
    r = rospy.Rate(10)
    r.sleep()

def checkEDT(traj_filepath, collision_fn):

    with open(traj_filepath, 'r') as file_open:
        traj = yaml.load(file_open)

    #print(traj)
    traj_separation_dists = []
    EDTGood = False
    num_states = len(traj.joint_trajectory.points)
    for pt in traj.joint_trajectory.points:
        q = pt.positions
        _, link_dists, _ = collision_fn(CONF.PandaConfiguration(list(q)))
        min_link_dist = link_dists[-1]
        traj_separation_dists.append(min_link_dist)

    traj_separation_dists = [i for i in traj_separation_dists if (not ((i < 0.01) and (i > 0.01*0.99)) and i != 2.5 and i != -2.5)]

    #print(traj_separation_dists)
    print(num_states)
    EDTGood = (num_states == len(traj_separation_dists))
    print(len(traj_separation_dists))

    print("Is EDT good?: " + str(EDTGood))

    return EDTGood

def gripper_open(hand_moveit):
    joint_goal = hand_moveit.get_current_joint_values()
    joint_goal[0] = 0.04
    joint_goal[1] = 0.04
    plan = hand_moveit.plan(joint_goal)
    if plan.joint_trajectory.points:
        hand_moveit.go()
        print("OPEN GRIPPER")
    

def gripper_close(hand_moveit):
    joint_goal = hand_moveit.get_current_joint_values()
    joint_goal[0] = 0.02
    joint_goal[1] = 0.02
    plan = hand_moveit.plan(joint_goal)
    if plan.joint_trajectory.points:
        hand_moveit.go()
        print("OPEN GRIPPER")

if __name__ == "__main__":
    rospy.init_node(NODE_NAME)
    rospack = rospkg.RosPack()

    moveit_commander.roscpp_initialize(sys.argv)
    robot = moveit_commander.RobotCommander()
    panda_arm_moveit = moveit_commander.MoveGroupCommander("panda_arm")
    hand_moveit = moveit_commander.MoveGroupCommander("hand")
    gripper_open(hand_moveit)

    if CLUTTERED:
        from panda_human_configs_ps2 import *
    else:
        from panda_human_configs import *

    if HANDOVER:
        print("Handover settings")
        home_config = [-0.8119078293769544, -0.11147239680113508, 0.0941664741045658, -2.3221939794217388, 0.012914459319253169, 2.20908192485844, 0.058717333826400875]
        
        print("Planning to 'home_config'")
        plan = panda_arm_moveit.plan(home_config)
        panda_arm_moveit.execute(plan)
        
        gripper_close(hand_moveit)
        scene = moveit_commander.PlanningSceneInterface()
        touch_links = robot.get_link_names(group = 'hand')
        touch_links.append("drill__link")
        ee_link = panda_arm_moveit.get_end_effector_link()
        rospy.sleep(2.0)
        scene.attach_mesh(panda_arm_moveit.get_end_effector_link(),"drill__link", touch_links)
        rospy.sleep(2.0)
        scene.remove_world_object("cabinet__cabinet_bottom_plate")
        goal_configs = [[0.5195776139247315, -0.3107862878803278, -0.40660219964913935, -1.8909993507330585, -0.11730471227448191, 1.5909125173477108, -1.8823763561159073]]#[panda_js[2][2:]] #use willow garage v4
        human_configs = [[0.828000000000003, 0.8880000000000017, 1.0620000000000047, 0.0, 0.0, -3.1405499999999997, 0.015707963267900027, 0.17278759594690007, 0.0, 0.0, 0.36128315516169995, 0.12566370614320022, 0.8168140899308001, 0.23561944902080012, -0.5811946409123, 1.08384946548855, 0.1806415775797, -0.6597344572518, -1.2880529879678, 0.0, -1.520138145254958, -0.21991148575059993, -0.5497787143782502, 0.07068583470510004, 0.14137166941110002, -1.5079644737184, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0]]
    else:
        human_configs =  human_js #[human_js[2]]
        home_config = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]
        goal_configs = [config[2:] for config in panda_js] #use willow garage v4
        print("Planning to 'home_config'")
        plan = panda_arm_moveit.plan(home_config)
        panda_arm_moveit.execute(plan)
        gripper_close(hand_moveit)

    #these are just notes below: set this up properly afterwards
    # if "dc":
    #     rrt_connect_metadata["W_DC"] = 1.0
    #     rrt_connect_metadata["W_VIS"] = 0.0
    #     rrt_connect_metadata["W_DIST"] = 0.0

    #     human_configs =  human_js
    #     home_config = [0,0,0,-0.0698,0,-0.0175,0.9124]
    #     goal_configs = [panda_js[1][2:]]
    # if "vis":
        # rrt_connect_metadata["W_DC"] = 0.0
        # rrt_connect_metadata["W_VIS"] = 1.0
        # rrt_connect_metadata["W_DIST"] = 0.0

        # human_configs =  [human_js[2]]
        # home_config = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]
        # goal_configs = [panda_js[0][2:]]
    # if "dist":
        # rrt_connect_metadata["W_DC"] = 0.0
        # rrt_connect_metadata["W_VIS"] = 0.0
        # rrt_connect_metadata["W_DIST"] = 1.0
        # human_configs =  [human_js[0]]
        # home_config = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]
        # goal_configs = [panda_js[0][2:]]
    # if "general":


    human_joint_states_pub = rospy.Publisher('/human_1/set_joint_states', Float64MultiArray, queue_size=10)
    reset_edt_pub = rospy.Publisher("/human_model/reset_edt", Empty, queue_size=10)

    rospy.Subscriber("/human_model/com", Vector3, human_com_callback)
    spinOnce() #update human_com variable for use during tests

    np.random.seed(int(time.time()))
    pin.seed(int(time.time()))

    pmig = PandaModelInfoGeneration()

    cs = CONF.PandaConfigurationSpace(pmig.get_pin_model())
    coll = STAT_COLL.CollisionChecker(cs)

    #this returns validity of config AND human model data such as separation distance to links + visibility
    collision_check_and_human_model_data = coll.check_config_dist_human_model

    fk = ForwardKinematics()
    tpc = TimeParametrizationClient()
    jtc = JointTrajectoryClient()
    traj_utils = TrajectoryUtils()
    traj_saver = TrajectorySaver(TEST_PKG_NAME)
    metrics_plotter = MetricsPlotter(TEST_PKG_NAME)

    traj_metrics_rrtc = TrajectoryMetricsGenerator(TEST_PKG_NAME, fk, compute_cost, collision_check_and_human_model_data, pmig.generate_info, rrt_connect_metadata, human_com)
    traj_metrics_rrtcc = TrajectoryMetricsGenerator(TEST_PKG_NAME, fk, compute_cost, collision_check_and_human_model_data, pmig.generate_info, rrt_connect_metadata, human_com)
    
    traj_metrics_rrtc.write_metadata_to_file(rrt_connect_metadata, rrt_connect_metadata_dir_path, rrt_connect_metadata_file_name)
    traj_metrics_rrtcc.write_metadata_to_file(rrt_connect_metadata, rrt_connect_costmap_metadata_dir_path, rrt_connect_costmap_metadata_file_name)
    
    
    print("================================")
    print("Testing collision checker and human model data")
    _, link_dists, _ = collision_check_and_human_model_data(CONF.PandaConfiguration(home_config))
    print("Separation distance from panda_gripper_center to human: " + str(link_dists[-1])) #link_dists[-1] is "panda_gripper_center"

    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                            DisplayTrajectory,
                                            queue_size=20)

    pkg_dir = rospack.get_path('human_aware_planning')
    urdf = pkg_dir + "/models/" + 'panda_arm.urdf'
    model = pin.buildModelFromUrdf(urdf)
    print("Pinocchio Panda URDF model loaded")

    #markers for drawing RRT tree expansion
    rviz_rrt_expansion_markers = rviz_tools.RvizMarkers('/panda_link0', 'rrt_expansion')

    #markers for drawing EE path, also draws start and end configuration in panda_girpper_center frame
    rviz_ee_path_markers = rviz_tools.RvizMarkers('/panda_link0', 'path_tracer')

    rviz_rrt_expansion_markers.deleteAllMarkers()
    rviz_ee_path_markers.deleteAllMarkers()

    iteration = 0
    while iteration < NUM_RUNS:
        print("Current run: " + str(iteration))
        """
        Execute any human position here
        """
        human_joints = Float64MultiArray()
        human_joints.data = human_configs[iteration % len(human_configs)] #human_configs[0]
        human_joint_states_pub.publish(human_joints)
        
        rospy.Subscriber("/human_model/com", Vector3, human_com_callback)
        spinOnce() #update human_com variable for use during tests

        """
        Update the distance map here
        """
        reset_edt_pub.publish() #publish Empty message to reset EDT
        rospy.sleep(8.0) #takes roughly 8s for EDT to be computed at 0.02 octomap resolution

        #update human CoM in metrics generation
        traj_metrics_rrtc.human_com = human_com
        traj_metrics_rrtcc.human_com = human_com

        goal_configs_index = iteration % len(goal_configs)

        if EXECUTE_TRAJECTORY:
            #plan to start position with MoveIt
            print("Planning to start position")
            plan = panda_arm_moveit.plan(home_config)
            panda_arm_moveit.execute(plan, wait=True)
            rospy.sleep(EXECUTE_TRAJECTORY_WAIT)

        if ENABLE_VISUALIZATIONS:
            #draw start and goal configurations in panda_gripper_center frame as spheres
            start_pose = fk.getFK('panda_gripper_center', PANDA_JOINT_NAMES, home_config, 'panda_link0').pose_stamped[0].pose.position
            goal_pose = fk.getFK('panda_gripper_center', PANDA_JOINT_NAMES, goal_configs[goal_configs_index], 'panda_link0').pose_stamped[0].pose.position
            start_point = Point(start_pose.x, start_pose.y, start_pose.z)
            goal_point = Point(goal_pose.x, goal_pose.y, goal_pose.z)
            diameter = 0.03
            rviz_ee_path_markers.publishSphere(start_point, 'red', diameter)
            rviz_ee_path_markers.publishSphere(goal_point, 'blue', diameter)

        start = CONF.PandaConfiguration(np.array(home_config))
        end = CONF.PandaConfiguration(np.array(goal_configs[goal_configs_index]))

        print("================================")
        print("Trying to plan with RRTConnect:")
        t0 = rospy.Time.now() 
        rrtconnect_path, rrtconnect_path_cost, rrtconnect_num_nodes = RRT_CONNECT_PLANNER(start, 
        end, cs, collision_check_and_human_model_data, pmig.generate_info, rviz_rrt_expansion_markers, apply_smoothing=True)

        rrtconnect_planning_time = (rospy.Time.now() - t0).to_sec()
        print(str(rrtconnect_planning_time) + " seconds elapsed planning from home to second ")
        print("Path cost: " + str(rrtconnect_path_cost))

        traj_metrics_rrtc.update_num_nodes_list(rrtconnect_num_nodes)
        traj_metrics_rrtc.update_planning_time_list(rrtconnect_planning_time)
        
        init_traj = None
        # init_traj = traj_utils.create_trajectory(model, rrtconnect_path)

        #make sure path has N_POINTS
        if len(rrtconnect_path) > N_POINTS:
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)
            init_traj = traj_utils.n_points_filter(init_traj, N_POINTS)
        elif len(rrtconnect_path) < N_POINTS:
            step = 0.01
            while len(rrtconnect_path) < N_POINTS:
                rrtconnect_path = rediscretize_path(cs, rrtconnect_path, step)
                step = float(step/2)
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)
            init_traj = traj_utils.n_points_filter(init_traj, N_POINTS)
        else:
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)

        _, traj = tpc.parametrize_trajectory(init_traj)

        if ENABLE_VISUALIZATIONS:
            draw_ee_traj(traj, RRTCONNECT_PATH_COLOUR, rviz_ee_path_markers, fk)

            if DISPLAY_TRAJ:
                robot_start = robot.get_current_state()
                display_trajectory = create_display_trajectory(robot_start, traj)
                display_trajectory_publisher.publish(display_trajectory)

        if EXECUTE_TRAJECTORY:
            jtc.add_full_trajectory(traj.joint_trajectory)
            jtc.start()
            jtc.wait()
            rospy.sleep(EXECUTE_TRAJECTORY_WAIT)

        #save trajectory in case processing/metrics need to be regathered
        traj_file_name = "traj_" + str(iteration) + ".yaml"
        # traj_saver.save_trajectory(traj, RELATIVE_PATH_TRAJ_RRTC, traj_file_name)
        #gather metrics
        traj_metrics_rrtc.generate_metrics(traj)

        if EXECUTE_TRAJECTORY:
            #plan to start position with MoveIt
            print("Planning to start position")
            plan = panda_arm_moveit.plan(home_config)
            panda_arm_moveit.execute(plan, wait=True)
            rospy.sleep(EXECUTE_TRAJECTORY_WAIT)

        #clear any markers
        #rviz_rrt_expansion_markers.deleteAllMarkers()
        #rviz_ee_path_markers.deleteAllMarkers()

        print("================================")
        print("Trying to plan with RRTConnect-Costmap:")
        t0 = rospy.Time.now() 
        rrtconnect_path, rrtconnect_path_cost, rrtconnect_num_nodes = RRT_CONNECT_COSTMAP_PLANNER(start, 
        end, cs, coll.check_config_dist_human_model, pmig.generate_info, rviz_rrt_expansion_markers, human_com=human_com, cost_params = rrt_connect_metadata, draw_expansion=False, apply_smoothing=True)

        rrtconnect_planning_time = (rospy.Time.now() - t0).to_sec()
        print(str(rrtconnect_planning_time) + " seconds elapsed planning from home to second ")
        print("Path cost: " + str(rrtconnect_path_cost))
        
        traj_metrics_rrtcc.update_num_nodes_list(rrtconnect_num_nodes)
        traj_metrics_rrtcc.update_planning_time_list(rrtconnect_planning_time)

        init_traj = None
        #make sure path has N_POINTS
        if len(rrtconnect_path) > N_POINTS:
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)
            init_traj = traj_utils.n_points_filter(init_traj, N_POINTS)
        elif len(rrtconnect_path) < N_POINTS:
            step = 0.01
            while len(rrtconnect_path) < N_POINTS:
                rrtconnect_path = rediscretize_path(cs, rrtconnect_path, step)
                step = float(step/2)
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)
            init_traj = traj_utils.n_points_filter(init_traj, N_POINTS)
        else:
            init_traj = traj_utils.create_trajectory(model, rrtconnect_path)

        _, traj = tpc.parametrize_trajectory(init_traj)

        if ENABLE_VISUALIZATIONS:
            draw_ee_traj(traj, RRTCONNECT_COSTMAP_PATH_COLOUR, rviz_ee_path_markers, fk)

            if DISPLAY_TRAJ:
                robot_start = robot.get_current_state()
                display_trajectory = create_display_trajectory(robot_start, traj)
                display_trajectory_publisher.publish(display_trajectory)

        if EXECUTE_TRAJECTORY:
            jtc.add_full_trajectory(traj.joint_trajectory)
            jtc.start()
            jtc.wait()
            rospy.sleep(EXECUTE_TRAJECTORY_WAIT)

        #save trajectory in case processing/metrics need to be regathered
        traj_file_name = "traj_" + str(iteration) + ".yaml"
        # traj_saver.save_trajectory(traj, RELATIVE_PATH_TRAJ_RRTCC, traj_file_name)
        #gather metrics
        traj_metrics_rrtcc.generate_metrics(traj)


        #clear any markers
        rviz_rrt_expansion_markers.deleteAllMarkers()
        rviz_ee_path_markers.deleteAllMarkers()
        
        iteration += 1

    #write metrics to files
    traj_metrics_rrtc.write_all_metrics_to_files(rrt_connect_metrics_dir_path)
    traj_metrics_rrtcc.write_all_metrics_to_files(rrt_connect_costmap_metrics_dir_path)


