    #plot metrics
    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_min_dist, rrtc_min_dist, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Distance (m)', 'Minimum Clearance Distance Comparison', '/' + TEST_NAME + '/','min_dist.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_dist, rrtc_avg_dist, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Distance (m)', 'Average Clearance Distance Comparison', '/' + TEST_NAME + '/','avg_dist.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_path_length, rrtc_avg_path_length, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Distance (m)', 'Average Path Length Comparison', '/' + TEST_NAME + '/', 'avg_path_length.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_planning_time, rrtc_avg_planning_time, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Time (s)', 'Average Planning Time Comparison', '/' + TEST_NAME + '/', 'avg_planning_time.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_percent_traj_cone_gaze, rrtc_percent_traj_cone_gaze, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Percentage of Traj. within Gaze Cone', 'Trajectory Visibility Comparison', '/' + TEST_NAME + '/', 'traj_vis.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_nodes, rrtc_avg_nodes, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Number of Nodes', 'Number of Nodes Comparison', '/' + TEST_NAME + '/', 'num_nodes.png')

    # metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_traj_inertia, rrtc_avg_traj_inertia, 'RRT-Connect Costmap', 
    #     'RRT-Connect', 'Avg. Highest Eig. of 3x3 Inertia Tensor', 'Avg. Highest Eig. of Inertia Tensor Comparison', '/' + TEST_NAME + '/', 'avg_inertia.png')




        #copy metrics into short variable names
    rrtc_min_dist = traj_metrics_rrtc.min_separation_dist_
    rrtcc_min_dist = traj_metrics_rrtcc.min_separation_dist_

    rrtc_avg_dist = traj_metrics_rrtc.avg_separation_dist_
    rrtcc_avg_dist = traj_metrics_rrtcc.avg_separation_dist_

    rrtc_avg_path_length = traj_metrics_rrtc.path_length_
    rrtcc_avg_path_length = traj_metrics_rrtcc.path_length_

    rrtc_avg_planning_time = traj_metrics_rrtc.planning_time_
    rrtcc_avg_planning_time = traj_metrics_rrtcc.planning_time_

    rrtc_percent_traj_cone_gaze = traj_metrics_rrtc.percent_within_gaze_cone_
    rrtcc_percent_traj_cone_gaze = traj_metrics_rrtcc.percent_within_gaze_cone_

    rrtc_avg_nodes = traj_metrics_rrtc.num_nodes_
    rrtcc_avg_nodes = traj_metrics_rrtcc.num_nodes_

    rrtc_avg_traj_inertia = traj_metrics_rrtc.avg_inertia_
    rrtcc_avg_traj_inertia = traj_metrics_rrtcc.avg_inertia_

    #plot metrics
    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_min_dist, rrtc_min_dist, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Distance (m)', 'Minimum Clearance Distance Comparison', '/' + NODE_NAME + '/','min_dist.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_dist, rrtc_avg_dist, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Distance (m)', 'Average Clearance Distance Comparison', '/' + NODE_NAME + '/','avg_dist.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_path_length, rrtc_avg_path_length, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Distance (m)', 'Average Path Length Comparison', '/' + NODE_NAME + '/', 'avg_path_length.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_planning_time, rrtc_avg_planning_time, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Time (s)', 'Average Planning Time Comparison', '/' + NODE_NAME + '/', 'avg_planning_time.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_percent_traj_cone_gaze, rrtc_percent_traj_cone_gaze, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Percentage of Traj. within Gaze Cone', 'Trajectory Visibility Comparison', '/' + NODE_NAME + '/', 'traj_vis.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_nodes, rrtc_avg_nodes, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Number of Nodes', 'Number of Nodes Comparison', '/' + NODE_NAME + '/', 'num_nodes.png')

    metrics_plotter.plot_two_dataset_avg_errs(rrtcc_avg_traj_inertia, rrtc_avg_traj_inertia, 'RRT-Connect Costmap', 
        'RRT-Connect', 'Avg. Highest Eig. of 3x3 Inertia Tensor', 'Avg. Highest Eig. of Inertia Tensor Comparison', '/' + NODE_NAME + '/', 'avg_inertia.png')


# home_config = [9.396968408559303e-05, -0.34242816458374925, -0.0027769118186089514, -0.6560788210725264, -0.003087934460905828, 0.30172427004212565, 0.7742325743042464]
    goal_configs = {} 

    #use this goal configuration from the high inertia one -> inertia along traj is reduced
    #goal_configs[0] = [-0.214951633953941, -1.7628001749137319, 1.3735977993497812, -2.1573995716600596, 1.6315972648450066, 1.290577536651404, 1.577636321645973]
    
    goal_configs[0] = [-0.12185887043295907, -1.0939445981077949, 1.3915225437641618, -2.1140595089153678, 1.0732695384268007, 1.668289738224785, 1.6730963992720618]
    goal_configs[1] = [-0.0017054716930893221, 0.7617314346153705, -0.21946473027250502, -0.5511224983336902, 0.15896928756036655, 1.2874114838837754, 0.6700715933310093]
    goal_configs[2] = [0.1784301838345419, -1.7628002939989855, 0.38443906824710794, -3.041811759841356, 0.3963399621863246, 1.2732650230096327, 0.9977624450202027]
    goal_configs[3] = [-1.4437144457773377, 0.4207166147871728, 1.488508629387641, -1.8446185703401579, 0.959517584458057, 2.3675102369327945, -0.8545423512355486]
    goal_configs[0] = [-0.05957047179034536, 0.4726751744006439, 0.11664893791994668, -1.4189268140963884, 1.355399584755089, 2.2737551297641154, -0.9203586793286869]

    goal_configs[0] = [2.366590274178826, -1.7622345617634547, -1.3971819968391275, -1.3176907432069118, -1.870485984307157, 1.577903774842511, 1.2939845756386967]




        human_configs[0] = [0.95, 0.82, 1.07, 0.0, 0.0, -3.14, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.35, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.52, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    #human standing in front of robot
    human_configs[1] = [0.2579999999999991, 0.8340000000000032, 1.0680000000000014, 0.0, 0.0, 1.512, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.3298672286258999, 1.5511613727091, 0.12566370614320022, 1.39800873084755, 0.0, 0.0, 0.0, 0.0, 0.11780972450740013, -0.06283185307160011, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0]
    
    #Human arms out looking away, used for viscos test
    human_configs[2] = [0.9480000000000004, 0.8159999999999954, 1.0680000000000014, 0.0, 0.0, -3.1405499999999997, 0.0, 0.0, 0.0, 0.0, 0.3455751918937999, 0.2513274122864, 0.9267698328061003, 0.7461282552282, -0.10995574287529997, 0.7382742735936499, 0.0, 0.0, 0.0, 0.9896016858776999, -0.8050331174829, -0.5811946409123, -0.4084070449667001, -0.05497787143729993, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0]

    human_configs[3] = [0.828000000000003, 1.0680000000000014, 1.0680000000000014, 0.0, 0.0, -3.1405499999999997, -0.377933596225674, 0.255725642001412, 0.36442474781528, -0.004398229715012025, 0.0, 0.0, 1.0763096431165082, 1.5574445580162681, 0.16210618092472795, 0.891898154354201, 0.0, 0.0, 0.0, 0.0, -1.520138145254958, 0.0, -0.7068583470577501, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0]

    human_configs[4] = []

    #use for when human is standing right in front of robot
    #goal_configs[0] = [-0.1822817380429127, -1.2821494559246185, 0.06406504152237957, -2.5472279650467966, 0.06272294609429352, 1.2539390659583445, 0.6346461684699038]

    #EDTOkay = checkEDT(rospack.get_path(TEST_PKG_NAME) + "/scripts/data/traj_for_EDT_check.yaml", collision_check_and_human_model_data)