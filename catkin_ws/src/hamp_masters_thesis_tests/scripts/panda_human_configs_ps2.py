#!/usr/bin/env python

import os
import sys
sys.setrecursionlimit(10000) #to avoid recursion limit errors
import time
import pinocchio as pin
import numpy as np
import random

import rospkg
import rospy
import moveit_commander
from moveit_msgs.msg import DisplayTrajectory, RobotTrajectory, RobotState, DisplayRobotState
from geometry_msgs.msg import Point, Vector3
from std_msgs.msg import Empty, Header, Float64MultiArray
from sensor_msgs.msg import JointState

panda_js = []
panda_js.append([0,0,-0.12185887043295907, -1.0939445981077949, 1.3915225437641618, -2.1140595089153678, 1.0732695384268007, 1.668289738224785, 1.6730963992720618])
panda_js.append([0,0, -0.943200638654246, -1.6150500659085312 + 0.75, 2.142995100073808, -2.7973894946140065 - 0.2, 0.5537782617351485, 3.4085972212377227, 0.26213612302675315 + 0.8])
panda_js.append([0,0,-0.3186988729204998, 0.4360672681674034, -0.16310999319079134, -1.0049524912420784, 0.06919134471095578, 1.4337897766140708, 0.32772532859923853])
panda_js.append([0,0,-1.9845558106181391, 0.9530368306279335, 1.4956033066889365, -2.0290267056409688, 0.6310822791141817, 1.542700571883704, -2.6569244620284174])
panda_js.append([0,0,2.366590274178826, -1.7622345617634547, -1.3971819968391275, -1.3176907432069118, -1.870485984307157, 1.577903774842511, 1.2939845756386967])


human_js = []
human_js.append([0.828000000000003, 1.0680000000000014, 1.0680000000000014, 0.0, 0.0, -3.1405499999999997, -0.377933596225674, 0.255725642001412, 0.36442474781528, -0.004398229715012025, 0.0, 0.0, 1.0763096431165082, 1.5574445580162681, 0.16210618092472795, 0.891898154354201, 0.0, 0.0, 0.0, 0.0, -1.520138145254958, 0.0, -0.7068583470577501, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0])
human_js.append([0.8399999999999999, 0.828000000000003, 1.0680000000000014, 0.0, 0.0, -3.1405499999999997, -0.0018849555921480743, 0.47752208334416, -0.10115928344527592, 0.0, 0.0, 0.0, 0.8944114284742257, 1.665829504564916, 0.18943803701087414, 0.43825217517580506, 0.30284953180318386, 0.0, 0.0, 1.089818491526902, -1.75732839060055, 0.016022122533257965, -0.5877919854866893, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0])
human_js.append([0.8220000000000027, 0.6360000000000028, 1.0680000000000014, 0.0, 0.0, -3.1405499999999997, 0.221796441342748, 0.6430840161878262, -0.0876504350348819, 0.015707963267900027, -0.3355220954023441, -0.36442474781528, 0.11812388377460792, 0.5234678759054341, 1.0781945987086559, 1.627659153824979, 0.25446900493836, -0.310075194908346, -0.2541548456746221, 0.1589645882711479, -1.818982146427136, 0.1979203371755398, -0.528415884333838, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0, 0.0, 0.0, 0.0, -0.00016521299593108552, 0.0, 0.0])

# Set the color of an object
def create_object_color(self, name, r, g, b, a = 0.9):
    # Initialize a MoveIt color object
    color = ObjectColor()
    
    # Set the id to the name given as an argument
    color.id = name
    
    # Set the rgb and alpha values given as input
    color.color.r = r
    color.color.g = g
    color.color.b = b
    color.color.a = a
    
    return color

def display_robot_state(joint_config, drs_topic):
    rospy.loginfo("Getting a current joint states message")
    js = rospy.wait_for_message('/joint_states', JointState)
    js.position = joint_config

    rs_pub = rospy.Publisher(drs_topic, DisplayRobotState, queue_size=1)
    rospy.sleep(0.3) # let it initialize...

    rs = RobotState()
    rs.joint_state = js
    drs = DisplayRobotState()
    drs.state = rs
    rs_pub.publish(drs)
    rospy.loginfo("Published current robot state")
    rospy.sleep(2.0)

def display_human_state(joint_config, dhs_topic):
    rospy.loginfo("Getting a current human joint states message")
    js = rospy.wait_for_message('/human_1/joint_states', JointState)
    js.position = joint_config

    rs_pub = rospy.Publisher(dhs_topic, DisplayRobotState, queue_size=1)
    rospy.sleep(0.3) # let it initialize...

    rs = RobotState()
    rs.joint_state = js
    drs = DisplayRobotState()
    drs.state = rs
    rs_pub.publish(drs)
    rospy.loginfo("Published current robot state")
    rospy.sleep(2.0)

if __name__ == '__main__':
    rospy.init_node("panda_human_configs_figure")

    
    display_robot_state(panda_js[0], "drs0")
    display_robot_state(panda_js[1], "drs1")
    display_robot_state(panda_js[2], "drs2")
    display_robot_state(panda_js[3], "drs3")
    display_robot_state(panda_js[4], "drs4")


    display_human_state(human_js[0], "dhs0")
    display_human_state(human_js[1], "dhs1")
    display_human_state(human_js[2], "dhs2")
