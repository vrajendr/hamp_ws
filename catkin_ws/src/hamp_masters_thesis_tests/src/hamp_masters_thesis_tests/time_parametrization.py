
#!/usr/bin/env python
import rospy
from std_msgs.msg import Header
from hamp_ros.srv import TimeParametrization

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from moveit_msgs.msg import RobotState, RobotTrajectory

class TimeParametrizationClient():
    """Lightweight wrapper for Time Parametrization service"""

    def __init__(self):
        rospy.loginfo("Loading Time Parametrization Wrapper client.")
        self.time_parametrization_srv = rospy.ServiceProxy('/move_group/add_time_parametrization', TimeParametrization)
        rospy.loginfo("Connecting to Time Parametrization service")
        self.time_parametrization_srv.wait_for_service()
        rospy.loginfo("Ready to Time Parametrize trajectories")

        self.arm_joints = ["panda_joint1",
                    "panda_joint2", 
                    "panda_joint3", 
                    "panda_joint4", 
                    "panda_joint5",
                    "panda_joint6",
                    "panda_joint7"]

    def parametrize_trajectory(self, traj):

        if not isinstance(traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory")
        try:
            joint_state = JointState()
            joint_state.header = Header()
            joint_state.header.stamp = rospy.Time.now()
            joint_state.name = self.arm_joints
            joint_state.position = traj.points[0].positions
            moveit_robot_state = RobotState()
            moveit_robot_state.joint_state = joint_state

            moveit_robot_traj = RobotTrajectory()
            moveit_robot_traj.joint_trajectory = traj

            resp = self.time_parametrization_srv(moveit_robot_state, moveit_robot_traj, "panda_arm")
            return resp.success, resp.parametrized_trajectory
        except rospy.ServiceException, e:
            print("Service call failed: %s"%e)