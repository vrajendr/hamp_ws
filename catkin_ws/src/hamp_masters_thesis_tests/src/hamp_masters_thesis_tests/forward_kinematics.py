#!/usr/bin/env python
import rospy
from moveit_msgs.srv import GetPositionFK, GetPositionFKRequest, GetPositionFKResponse
DEFAULT_FK_SERVICE = "/compute_fk"

class ForwardKinematics():
    """Simplified interface to ask for forward kinematics"""

    def __init__(self):
        rospy.loginfo("Loading ForwardKinematics class.")

        self.arm_joints = ["panda_joint1",
                            "panda_joint2", 
                            "panda_joint3", 
                            "panda_joint4", 
                            "panda_joint5",
                            "panda_joint6",
                            "panda_joint7"]

        self.fk_srv = rospy.ServiceProxy(DEFAULT_FK_SERVICE, GetPositionFK)
        rospy.loginfo("Connecting to FK service")
        self.fk_srv.wait_for_service()
        rospy.loginfo("Ready for making FK calls")



    def closeFK(self):
        self.fk_srv.close()

    def getFK(self, fk_link_names, joint_names, positions, frame_id='panda_link0'):
        """Get the forward kinematics of a joint configuration
        @fk_link_names list of string or string : list of links that we want to get the forward kinematics from
        @joint_names list of string : with the joint names to set a position to ask for the FK
        @positions list of double : with the position of the joints
        @frame_id string : the reference frame to be used"""
        gpfkr = GetPositionFKRequest()
        if type(fk_link_names) == type("string"):
            gpfkr.fk_link_names = [fk_link_names]
        else:
            gpfkr.fk_link_names = fk_link_names
        gpfkr.robot_state.joint_state.name = joint_names
        gpfkr.robot_state.joint_state.position = positions
        gpfkr.header.frame_id = frame_id
        # fk_result = GetPositionFKResponse()
        fk_result = self.fk_srv.call(gpfkr)
        return fk_result
        #to read the actual joint angles: .pose_stamped[0].pose.position

    def getCurrentFK(self, fk_link_names, frame_id='base_link'):
        """Get the forward kinematics of a set of links in the current configuration"""
        # Subscribe to a joint_states
        js = rospy.wait_for_message('/joint_states', JointState)
        # Call FK service
        fk_result = self.getFK(fk_link_names, js.name, js.position, frame_id)
        return fk_result
        #to read the actual joint angles: .pose_stamped[0].pose.position

    
    def convert_traj_to_fk_poses(self, traj, link="panda_gripper_center", base_link="panda_link0"):
        #traj is trajectory_msgs/JointTrajectory

        fk_poses = []
        for i, pt in enumerate(traj.points): #traj_msg_converted.joint_trajectory.points # #repaired_traj.points
            fk_poses.append(self.getFK(link, self.arm_joints, pt.positions, base_link).pose_stamped[0].pose)
            rospy.sleep(0.05)

        return fk_poses