#!/usr/bin/env python

import rospy
import rospkg

import sys
import os
import errno
import pickle
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import copy
import numpy as np

from hamp_masters_thesis_tests.metrics_plotter import MetricsPlotter

def read_data_from_file(dir_path, filename):
    full_dir_path = dir_path
    full_path = full_dir_path + filename
    
    with open(full_path, 'rb') as f:
        data = pickle.load(f)

    return data


if __name__ == "__main__":
    rospy.init_node("generate_plots_from_files")
    rospack = rospkg.RosPack()

    myargv = rospy.myargv(argv=sys.argv)

    TEST_NAME = myargv[1]

    TEST_PKG_NAME = 'hamp_masters_thesis_tests'
    #TEST_NAME = "test_03"
    RELATIVE_PATH_TRAJ_RRTC = "/rrt/rrtconnect/"+ TEST_NAME +"/trajectories/"
    RELATIVE_PATH_TRAJ_RRTCC = "/rrt/rrtconnect_costmap/"+ TEST_NAME +"/trajectories/"

    rrt_connect_metadata_dir_path = "/rrt/rrtconnect/" + TEST_NAME + "/"
    rrt_connect_metadata_file_name = "rrt_connect_metadata.yaml"
    rrt_connect_costmap_metadata_dir_path = "/rrt/rrtconnect_costmap/" + TEST_NAME + "/"
    rrt_connect_costmap_metadata_file_name = "rrt_connect_costmap_metadata.yaml"

    rrt_connect_costmap_metrics_dir_path = "/rrt/rrtconnect_costmap/" + TEST_NAME + "/metrics/"
    rrt_connect_metrics_dir_path = "/rrt/rrtconnect/" + TEST_NAME + "/metrics/"

    hamp_masters_thesis_tests_dir = rospack.get_path(TEST_PKG_NAME)

    metrics_plotter = MetricsPlotter(TEST_PKG_NAME)

    #copy metrics into short variable names
    rrtc_traj_dists = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'traj_separation_dists_.pkl')
    rrtcc_traj_dists = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'traj_separation_dists_.pkl')

    p1_min_dist = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'min_separation_dist_.pkl')
    p2_min_dist = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'min_separation_dist_.pkl')

    p1_avg_dist = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'avg_separation_dist_.pkl')
    p2_avg_dist = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'avg_separation_dist_.pkl')

    p1_avg_path_length = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'path_length_.pkl')
    p2_avg_path_length = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'path_length_.pkl')

    p1_avg_planning_time = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'planning_time_.pkl')
    p2_avg_planning_time = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'planning_time_.pkl')

    p1_percent_traj_cone_gaze = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'percent_within_gaze_cone_.pkl')
    p2_percent_traj_cone_gaze = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'percent_within_gaze_cone_.pkl')

    p1_avg_nodes = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'num_nodes_.pkl')
    p2_avg_nodes = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'num_nodes_.pkl')

    p1_avg_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'avg_inertia_.pkl')
    p2_avg_traj_inertia = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'avg_inertia_.pkl')

    p1_integral_cost = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'path_integral_cost_.pkl')
    p2_integral_cost = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'path_integral_cost_.pkl')

    p1_mechanical_work = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_metrics_dir_path, 'path_mechanical_work_.pkl')
    p2_mechanical_work = read_data_from_file(hamp_masters_thesis_tests_dir + '/scripts/data' + rrt_connect_costmap_metrics_dir_path, 'path_mechanical_work_.pkl')


    #plot full trajectory separation dists
    metrics_plotter.plot_full_trajectory_metric(rrtcc_traj_dists, '/' + TEST_NAME + '/', "test.pdf", labelx="Waypoint Index", labely="Clearance (m)")

    data_labels = ['Human-Aware RRT-Connect', 'RRT-Connect']
    # titles = ['Min. Clearance', 'Avg. Clearance', 'Path Visibility','Path Length', 'Planning Time', 'Number of Nodes', 'Robot Inertia', 'Integral Cost', 'Mechanical Work']
    planner1 = [p1_min_dist,
                p1_avg_dist,
                p1_percent_traj_cone_gaze,
                p1_avg_path_length,
                p1_avg_planning_time,
                p1_avg_nodes,
                p1_avg_traj_inertia,
                p1_integral_cost,
                p1_mechanical_work
                ]
    
    planner2 = [p2_min_dist,
                p2_avg_dist,
                p2_percent_traj_cone_gaze,
                p2_avg_path_length,
                p2_avg_planning_time,
                p2_avg_nodes,
                p2_avg_traj_inertia,
                p2_integral_cost,
                p2_mechanical_work
                ]

    metrics_plotter.plot_all_metrics_subplots('/' + TEST_NAME + '/', "test2.pdf", data_labels, planner2, planner1)
