import rospy
import copy

from moveit_commander.conversions import pose_to_list
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

class TrajectoryUtils:

    #create JointTrajectory from a list of joint configurations
    def create_trajectory(self, model, path):
        #model should be from pinocchio

        joint_names = [str(n) for n in model.names]
        joint_names.pop(0) # 'universe'
        #joint_names.append("panda_finger_joint1")
        #joint_names.append("panda_finger_joint2")

        traj = JointTrajectory()
        traj.joint_names = joint_names

        i = 0
        for n in path:
            q = list(n)#list(n.conf.joints)

            pt = JointTrajectoryPoint()
            pt.positions = q
            traj.points.append(pt)

            i = i + 1
        return traj

    def n_points_filter(self, traj, n):
        #lazily downsample trajectory to have x number of points...
        n_points_ = n
        size_in = len(traj.points)

        intermediate_points = n_points_ - 2 #subtract the first and last elements
        int_point_increment = float(size_in) / float(intermediate_points + 1.0)
        final_traj = copy.deepcopy(traj)
        final_traj.points = [] #clear points
        final_traj.points.append(traj.points[0]) #add first point to trajectory

        i = 1
        while i <= intermediate_points:
            int_point_index = int(float(i) * int_point_increment)
            final_traj.points.append(traj.points[int_point_index])
            i += 1
        
        final_traj.points.append(traj.points[-1]) #add last point to trajectory

        return final_traj

    def moveit_retime(self, joint_trajectory, velocity_scale, moveit_interface, robot):
        return moveit_interface.retime_trajectory(robot.get_current_state(), joint_trajectory, velocity_scale)

    def remove_trajectory_vels(self, joint_trajectory):

        for i in range(0, len(joint_trajectory.points)):
            joint_trajectory.points[i].velocities = []

        return joint_trajectory

    def remove_trajectory_accels(self, joint_trajectory):

        for i in range(0, len(joint_trajectory.points)):
            joint_trajectory.points[i].accelerations = []

        return joint_trajectory

    def remove_trajectory_efforts(self, joint_trajectory):

        for i in range(0, len(joint_trajectory.points)):
            joint_trajectory.points[i].effort = []

        return joint_trajectory

    def remove_trajectory_vels_accels_eff(self, joint_trajectory):

        for i in range(0, len(joint_trajectory.points)):
            joint_trajectory.points[i].velocities = []
            joint_trajectory.points[i].accelerations = []
            joint_trajectory.points[i].effort = []
            
        return joint_trajectory

    def retime_traj(self, joint_trajectory, t):

        num_points = len(joint_trajectory.points)

        time_incr = t/num_points

        for i in range(0, num_points):
            joint_trajectory.points[i].time_from_start = rospy.Duration(time_incr)

        return joint_trajectory

    def retime_traj_and_remove_v_a_e(self, joint_trajectory, t):

        num_points = len(joint_trajectory.points)

        time_incr = rospy.Duration(float(t/num_points))

        for i in range(0, num_points):
            joint_trajectory.points[i].velocities = []
            joint_trajectory.points[i].accelerations = []
            joint_trajectory.points[i].effort = []
            if i == 0:
                joint_trajectory.points[i].time_from_start = rospy.Duration(0.0)
            else:
                joint_trajectory.points[i].time_from_start = joint_trajectory.points[i-1].time_from_start + time_incr
        
        return joint_trajectory

    def zero_trajectory_times(self, joint_trajectory):
        t_start = joint_trajectory.points[0].time_from_start.to_sec()

        for i in range(0, len(joint_trajectory.points)):
            t = joint_trajectory.points[i].time_from_start.to_sec()
            joint_trajectory.points[i].time_from_start = rospy.Duration(t - t_start)

        return joint_trajectory
    
    def split_trajectory_at_time(self, joint_trajectory, T):

        if T >= joint_trajectory.points[-1].time_from_start.to_sec():
            return joint_trajectory, None

        i = 0
        while joint_trajectory.points[i].time_from_start.to_sec() < T and i < len(joint_trajectory.points):
            i = i + 1

        traj_1 = copy.copy(joint_trajectory)
        traj_1.points = traj_1.points[0:i]
        traj_1.points[0].velocities = [0*v for v in traj_1.points[0].velocities]     
        traj_1.points[-1].velocities = [0*v for v in traj_1.points[0].velocities]     
        traj_1.points[0].accelerations = [0*v for v in traj_1.points[0].accelerations]     
        traj_1.points[-1].accelerations = [0*v for v in traj_1.points[0].accelerations]     


        traj_2 = copy.copy(joint_trajectory)
        traj_2.points = traj_2.points[i:-1]
        traj_2 = self.zero_trajectory_times(traj_2)
        traj_2.points[0].velocities = [0*v for v in traj_2.points[0].velocities]     
        traj_2.points[-1].velocities = [0*v for v in traj_2.points[0].velocities]     
        traj_2.points[0].accelerations = [0*v for v in traj_2.points[0].accelerations]     
        traj_2.points[-1].accelerations = [0*v for v in traj_2.points[0].accelerations]     

        return traj_1, traj_2

    def replace_traj_segs(self, traj_seg_idxs, traj_segs, joint_traj):
        #traj_segs should be 

        if len(traj_seg_idxs) == 0:
            return joint_traj

        num_traj_segs = len(traj_segs)

        new_traj = copy.deepcopy(joint_traj)

        num_states = len(joint_traj.points)

        #print(num_states)

        for i in range(0, num_traj_segs, 1):
            seg_idxs = traj_seg_idxs[i]
            idx_offset = len(new_traj.points) - num_states
            start = seg_idxs.values[0] + idx_offset
            end = seg_idxs.values[1] + idx_offset
            
            #Debugging
            #print (start)
            #print (end)

            for k in range(len(traj_segs[i].joint_trajectory.points)): 
                if (k+start) > end:
                    new_traj.points.insert(k + start, traj_segs[i].joint_trajectory.points[k])
                else:
                    new_traj.points[k + start] = traj_segs[i].joint_trajectory.points[k]

        new_traj = self.retime_traj_and_remove_v_a_e(new_traj, 3.0)

        return new_traj


    def get_trajectory_avg_speed(self, joint_trajectory):
        summ = 0.0
        n = 0.0

        for point in joint_trajectory.points:
            for v in point.velocities:
                summ = summ + abs(v)
                n = n + 1

        return summ/n

    def get_trajectory_avg_length(self, joint_trajectory):
        summ = 0.0
        n = 0.0

        for point in joint_trajectory.points:
            for p in point.positions:
                summ = summ + abs(p)
                n = n + 1

        return summ/n

    def get_trajectory_ee_length(self, joint_trajectory, fk_fn):
        NotImplementedError

    def reverse_robot_trajectory(self, trajectory):
        """
        Reverse the JointTrajectory inside a moveit_msgs/RobotTrajectory Message 
        and return a new moveit_msgs/RobotTrajectory Message. 
        
        :param traj: a moveit_msgs/RobotTrajectory Message
        """

        #Note: no Time Parameterization, just negate velocities

        traj = trajectory
        traj.joint_trajectory.points.reverse()
        end_t = traj.joint_trajectory.points[0].time_from_start
        for i, pt in enumerate(traj.joint_trajectory.points):
            pt.velocities = [-v for v in pt.velocities]
            pt.accelerations = []
            pt.effort = []
            pt.time_from_start = end_t - pt.time_from_start

        return traj 

    def check_trajectory(self):
        #each time_from_start in the traj should be greater than or equal to the previous
        raise NotImplementedError

