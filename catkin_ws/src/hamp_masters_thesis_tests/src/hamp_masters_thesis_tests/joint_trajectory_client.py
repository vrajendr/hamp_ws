#!/usr/bin/env python

# Copyright (c) 2013-2015, Rethink Robotics
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the Rethink Robotics nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""
Derived from Baxter RSDK Joint Trajectory Action Client Example
"""
import argparse
import sys

from copy import copy

import rospy

import actionlib

from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
)
from trajectory_msgs.msg import (
    JointTrajectory,
    JointTrajectoryPoint,
)

import moveit_commander

PANDA_JOINT_NAMES = ["panda_joint1", 
            "panda_joint2", 
            "panda_joint3", 
            "panda_joint4", 
            "panda_joint5",
            "panda_joint6",
            "panda_joint7"]

PANDA_HOME_POSITION = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]

class JointTrajectoryClient(object):
    def __init__(self):
        ns = 'panda_arm_controller/'
        self._client = actionlib.SimpleActionClient(
            ns + "follow_joint_trajectory",
            FollowJointTrajectoryAction,
        )
        self._goal = FollowJointTrajectoryGoal()
        self._goal_time_tolerance = rospy.Time(0.1)
        self._goal.goal_time_tolerance = self._goal_time_tolerance
        server_up = self._client.wait_for_server(timeout=rospy.Duration(10.0))
        if not server_up:
            rospy.logerr("Timed out waiting for Joint Trajectory"
                         " Action Server to connect. Start the action server"
                         " before running example.")
            rospy.signal_shutdown("Timed out waiting for Action Server")
            sys.exit(1)
        self.clear()

    def add_point(self, positions, time):
        point = JointTrajectoryPoint()
        point.positions = copy(positions)
        point.time_from_start = rospy.Duration(time)
        self._goal.trajectory.points.append(point)

    def add_points(self, traj):
        # traj should be trajectory_msgs/JointTrajectoryPoint
        # Note: this assumes that you've taken care of velocity, accel, times and continuity of the trajectory
        if not isinstance(traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory")

        for pt in traj.points:
            self._goal.trajectory.points.append(pt)

    def add_full_trajectory(self, traj):
        # traj should be trajectory_msgs/JointTrajectory
        # Note: this replaces the full trajectory, overwriting any past points
        # for pt in traj.points:
        if not isinstance(traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory")

        self._goal.trajectory.points = traj.points

    def get_trajectory(self):
        return self._goal

    def start(self):
        self._goal.trajectory.header.stamp = rospy.Time.now()
        self._client.send_goal(self._goal)

    def stop(self):
        self._client.cancel_goal()

    def wait(self, timeout=15.0):
        self._client.wait_for_result(timeout=rospy.Duration(timeout))

    def result(self):
        return self._client.get_result()

    def clear(self):
        self._goal = FollowJointTrajectoryGoal()
        self._goal.goal_time_tolerance = self._goal_time_tolerance
        self._goal.trajectory.joint_names = PANDA_JOINT_NAMES

def main():
    print("Initializing node... ")
    rospy.init_node("joint_trajectory_client_test")

    panda_arm = moveit_commander.MoveGroupCommander("panda_arm")

    jtc = JointTrajectoryClient()
    rospy.on_shutdown(jtc.stop)
    
    print("Planning to 'ready' position")
    plan = panda_arm.plan(PANDA_HOME_POSITION)

    jtc.add_full_trajectory(plan.joint_trajectory)
    #print(jtc.get_trajectory())

    jtc.start()
    jtc.wait(1.0)
    jtc.stop()
    print("Try to stop trajectory")
    print("Exiting - Joint Trajectory Action Test Complete")

if __name__ == "__main__":
    main()