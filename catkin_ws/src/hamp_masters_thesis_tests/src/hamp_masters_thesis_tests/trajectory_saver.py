#!/usr/bin/env python
import os
import errno
import rospkg
import yaml   
from moveit_msgs.msg import RobotTrajectory


class TrajectorySaver:
    def __init__(self, package_name):

        self.package = package_name
        rospack = rospkg.RosPack()
        self.pkg_dir = rospack.get_path(self.package)
        self.base_path = self.pkg_dir + "/scripts/data"

    def save_trajectory(self, traj, dir_path, filepath):
        #e.g. filepath should be relative to hamp_masters_thesis_tests: /rrt/rrtconnect_costmap/test_01/trajectories/traj_01.yaml

        if not isinstance(traj, RobotTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type moveit_msgs/RobotTrajectory")
        
        full_dir_path = self.base_path + dir_path
        full_path = self.base_path + dir_path + filepath

        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        with open(full_path, 'w+') as file_save:
            yaml.dump(traj, file_save, default_flow_style=True)