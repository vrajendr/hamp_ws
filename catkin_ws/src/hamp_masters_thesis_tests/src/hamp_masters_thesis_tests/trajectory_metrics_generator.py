#!/usr/bin/env python

import os
import errno
import pickle
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import copy
import numpy as np

import rospy
import rospkg
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from moveit_msgs.msg import RobotTrajectory

import human_aware_planning.panda_configuration_space as CONF

class TrajectoryMetricsGenerator:
    def __init__(self, package_name, fk, cost_fn, human_model_data, panda_info_gen, metadata, human_com):
        
        self.fk = fk
        self.compute_cost = cost_fn
        self.human_model_data = human_model_data
        self.panda_info_gen = panda_info_gen
        self.package = package_name
        rospack = rospkg.RosPack()
        self.pkg_dir = rospack.get_path(self.package)
        self.base_path = self.pkg_dir + "/scripts/data"
        self.human_com = human_com

        self.arm_joints = ["panda_joint1",
            "panda_joint2", 
            "panda_joint3", 
            "panda_joint4", 
            "panda_joint5",
            "panda_joint6",
            "panda_joint7"]

        #these will be lists of lists
        self.traj_separation_dists_ = []
        self.traj_vis_angles_ = []
        self.traj_inertias_ = []
        self.traj_com_ = []
        self.traj_human_com_ = []
        
        self.planning_time_ = []
        self.num_nodes_ = []

        self.min_separation_dist_ = []
        self.avg_separation_dist_ = []
        self.path_length_ = []
        self.percent_within_gaze_cone_ = []
        self.avg_inertia_ = []
        self.path_integral_cost_ = []
        self.path_mechanical_work_ = []

    def load_trajectory(self, filepath):

        with open(self.base_path + filepath, 'r') as file_open:
            traj = yaml.load(file_open, Loader=Loader)

        return traj 

    def compute_ee_path_length(self, traj):
        #traj should be trajectory_msgs/JointTrajectory

        if not isinstance(traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory")

        path_length = 0.0
        for i in range(len(traj.points)): #loop through all waypoints
            if i == len(traj.points) - 1:
                break

            current_pose_point = self.fk.getFK('panda_gripper_center', self.arm_joints, traj.points[i].positions, 'panda_link0').pose_stamped[0].pose.position
            next_pose_point = self.fk.getFK('panda_gripper_center', self.arm_joints, traj.points[i+1].positions, 'panda_link0').pose_stamped[0].pose.position
            current_pose = np.array([current_pose_point.x, current_pose_point.y, current_pose_point.z])
            next_pose = np.array([next_pose_point.x, next_pose_point.y, next_pose_point.z])
            path_length = path_length + np.linalg.norm(next_pose-current_pose)
            rospy.sleep(0.05)

        return path_length

    def path_integral_cost(self, joint_traj, path_length):
        #traj should be trajectory_msgs/JointTrajectory

        if isinstance(joint_traj, RobotTrajectory):
            traj = copy.deepcopy(joint_traj.joint_trajectory)
        elif not isinstance(joint_traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory or moveit_msgs/RobotTrajectory")

        n = len(traj.points)
        # path_length = self.compute_ee_path_length(traj)
        traj_cost_sum = 0
        for pt in traj.points:
            q = pt.positions
            _, link_dists, vis_angles, _ = self.human_model_data(CONF.PandaConfiguration(q))
            #dist = min(link_dists)

            cost = self.compute_cost(np.array(q), link_dists, vis_angles, self.panda_info_gen, self.human_com, w_dist=self.metadata["W_DIST"], 
                    w_vis=self.metadata["W_VIS"], w_inertia=self.metadata["W_INERTIA"], w_dc = self.metadata["W_DC"], min_dist = self.metadata["MIN_DIST"], min_CoM_dist = self.metadata["MIN_COM_DIST"])
            
            traj_cost_sum += cost

        
        integral_cost = path_length/n*traj_cost_sum

        return integral_cost

    def path_mechanical_work(self, joint_traj):

        #traj should be trajectory_msgs/JointTrajectory

        if isinstance(joint_traj, RobotTrajectory):
            traj = copy.deepcopy(joint_traj.joint_trajectory)
        elif not isinstance(joint_traj, JointTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type trajectory_msgs/JointTrajectory or moveit_msgs/RobotTrajectory")

        n = len(traj.points)
        traj_cost_sum = 0
        for i in range(1,n):
            q = traj.points[i].positions
            q_prev = traj.points[i-1].positions

            _, link_dists, vis_angles, _ = self.human_model_data(CONF.PandaConfiguration(q))
            q_cost = self.compute_cost(np.array(q), link_dists, vis_angles, self.panda_info_gen, self.human_com, w_dist=self.metadata["W_DIST"], 
                    w_vis=self.metadata["W_VIS"], w_inertia=self.metadata["W_INERTIA"], w_dc = self.metadata["W_DC"], min_dist = self.metadata["MIN_DIST"], min_CoM_dist = self.metadata["MIN_COM_DIST"])

            _, link_dists, vis_angles, _ = self.human_model_data(CONF.PandaConfiguration(q_prev))
            q_prev_cost = self.compute_cost(np.array(q_prev), link_dists, vis_angles, self.panda_info_gen, self.human_com, w_dist=self.metadata["W_DIST"], 
                    w_vis=self.metadata["W_VIS"], w_inertia=self.metadata["W_INERTIA"], w_dc = self.metadata["W_DC"], min_dist = self.metadata["MIN_DIST"], min_CoM_dist = self.metadata["MIN_COM_DIST"])

            traj_cost_sum += max(0, q_cost - q_prev_cost) + np.linalg.norm(np.array(q)-np.array(q_prev), 2)*0.0001

        return traj_cost_sum

    #maybe set compute_traj_cost boolean to False for PRM, it computes the cost using the RRT planning weights so not sure how valid 
    # it would be for evaluating the PRM
    def generate_metrics(self, traj, print_metrics=True, filepath=None, write_to_files=True, compute_traj_cost=True):
        
        if not isinstance(traj, RobotTrajectory):
            raise TypeError("Invalid trajectory type. Please make sure traj is of type moveit_msgs/RobotTrajectory")

        traj_separation_dists = []
        traj_vis_angles = []
        traj_inertias = []
        traj_com = []
        traj_human_com = []
        if compute_traj_cost:
            traj_costs = []
        num_states = len(traj.joint_trajectory.points)
        #weights = [0.5, 0.2, 0.3] #w_dist, w_vis, w_inertia (w_dc -> danger criterion)
        for pt in traj.joint_trajectory.points:
            q = pt.positions
            _, link_dists, vis_angles, _ = self.human_model_data(CONF.PandaConfiguration(list(q)))

            '''
            link_dists contains separation distance for each joint of the arm, but
            for the separation distance metric (traj_separation_dists) store
            the minimum distance across all links

            link_dists goes from: panda_link0 ... panda_link7, panda_gripper_center
            '''
            min_link_dist = min(link_dists)
            min_vis_angle = min(vis_angles[-1], vis_angles[3])

            traj_vis_angles.append(min_vis_angle)
            traj_separation_dists.append(min_link_dist)

            traj_inertias.append(self.panda_info_gen(np.asarray(q)).inertia)

            #Retrieve Panda and Human CoMs
            pmi = self.panda_info_gen(np.array(q))
            panda_com = pmi.center_of_mass
            human_com = self.human_com
            traj_com.append(panda_com.ravel().tolist()[0])
            traj_human_com.append(human_com)
        
        '''
        The EDT somestimes glitches and returns erroneous distance values. Filter them out from the separation distance list
        '''
        traj_separation_dists = [i for i in traj_separation_dists if (not ((i < 0.01) and (i > 0.01*0.99)) and i != 2.5 and i != -2.5)]

        '''
        Find the number of configurations along trajectory that are within the human gaze cone angle
        '''
        within_cone_of_gaze = 0.0
        cone_of_gaze_angle = 15 #was 30 before
        for angle in traj_vis_angles:
            #print(np.rad2deg(angle))
            #if you take the direction of gaze as 0 this would be +/- cone_of_gaze_angle. 
            # In this case, for the human model, the sign doesn't change, hence just check if we are less than cone_of_gaze_angle
            if angle <= np.deg2rad(cone_of_gaze_angle): 
                within_cone_of_gaze += 1.0

        percent_within_gaze_cone = float(within_cone_of_gaze/len(traj_vis_angles))

        path_length = self.compute_ee_path_length(traj.joint_trajectory)

        avg_separation_dist = sum(traj_separation_dists) / len(traj_separation_dists)
        min_separation_dist = min(traj_separation_dists)

        # if compute_traj_cost:
        #     traj_cost = sum(traj_costs)

        avg_inertia = sum(traj_inertias)/len(traj_inertias)

        if compute_traj_cost:
            path_integral_cost = self.path_integral_cost(traj, path_length)
            path_mechanical_work = self.path_mechanical_work(traj)
        else:
            path_integral_cost = 0
            path_mechanical_work = 0   

        if print_metrics:
            print("Num. states: " + str(num_states))
            print("Len. traj_separation_dists: " + str(len(traj_separation_dists)))
            print("Min. separation dist: " + str(min_separation_dist))
            print("Avg. separation dist: " + str(avg_separation_dist))
            print("Path length: " + str(path_length))
            print("Percentage of traj. within cone of gaze: " + str(percent_within_gaze_cone))
            print("Avg. of highest eig. of inertia tensor: " + str(avg_inertia))
            if compute_traj_cost:
                print("Path Integral Cost: " + str(path_integral_cost))
                print("Path Mechanical Work: " + str(path_mechanical_work))
        
        metrics = {"traj_separation_dists": traj_separation_dists,
                    "traj_vis_angles": traj_vis_angles,
                    "traj_inertias": traj_inertias,
                    "traj_com": panda_com,
                    "taj_human_com": human_com,
                    "min_separation_dist": min_separation_dist,
                    "avg_separation_dist": avg_separation_dist,
                    "path_length": path_length,
                    "percent_within_gaze_cone": percent_within_gaze_cone,
                    "avg_inertia": avg_inertia,
                    "path_integral_cost": path_integral_cost,
                    "path_mechanical_work": path_mechanical_work}
        
        self.traj_separation_dists_.append(traj_separation_dists)
        self.traj_vis_angles_.append(traj_vis_angles)
        self.traj_inertias_.append(traj_inertias)
        self.traj_com_.append(traj_com)
        self.traj_human_com_.append(traj_human_com)

        self.min_separation_dist_.append(min_separation_dist)
        self.avg_separation_dist_.append(avg_separation_dist)
        self.path_length_.append(path_length)
        self.percent_within_gaze_cone_.append(percent_within_gaze_cone)
        self.avg_inertia_.append(avg_inertia)
        self.path_integral_cost_.append(path_integral_cost)
        self.path_mechanical_work_.append(path_mechanical_work)

        return metrics
    
    def update_num_nodes_list(self, data):
        self.num_nodes_.append(data)

    def update_planning_time_list(self, data):
        self.planning_time_.append(data)

    def load_traj_and_gen_metrics(self, filepath):
        traj = self.load_trajectory(filepath)
        return self.generate_metrics(traj)

    def write_metadata_to_file(self, metadata, dir_path, filename):
        full_dir_path = self.base_path + dir_path
        full_path = self.base_path + dir_path + filename

        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        self.metadata = metadata
        with open(full_path, 'w+') as f:
            text = yaml.dump(metadata)
            f.write(text)

    #wanted to use human-readable csv format here but pickle is just easier for now...
    def write_all_metrics_to_files(self, dir_path):
        full_dir_path = self.base_path + dir_path

        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        
        with open(full_dir_path + "traj_separation_dists_.pkl", 'wb') as f:
            pickle.dump(self.traj_separation_dists_, f) 

        with open(full_dir_path + "traj_vis_angles_.pkl", 'wb') as f:
            pickle.dump(self.traj_vis_angles_, f) 

        with open(full_dir_path + "traj_inertias_.pkl", 'wb') as f:
            pickle.dump(self.traj_inertias_, f) 

        with open(full_dir_path + "traj_com_.pkl", 'wb') as f:
            pickle.dump(self.traj_com_, f) 
        
        with open(full_dir_path + "traj_human_com_.pkl", 'wb') as f:
            pickle.dump(self.traj_human_com_, f) 

        with open(full_dir_path + "min_separation_dist_.pkl", 'wb') as f:
            pickle.dump(self.min_separation_dist_, f) 

        with open(full_dir_path + "avg_separation_dist_.pkl", 'wb') as f:
            pickle.dump(self.avg_separation_dist_, f) 

        with open(full_dir_path + "path_length_.pkl", 'wb') as f:
            pickle.dump(self.path_length_, f) 

        with open(full_dir_path + "planning_time_.pkl", 'wb') as f:
            pickle.dump(self.planning_time_, f) 

        with open(full_dir_path + "num_nodes_.pkl", 'wb') as f:
            pickle.dump(self.num_nodes_, f) 

        with open(full_dir_path + "percent_within_gaze_cone_.pkl", 'wb') as f:
            pickle.dump(self.percent_within_gaze_cone_, f) 

        with open(full_dir_path + "avg_inertia_.pkl", 'wb') as f:
            pickle.dump(self.avg_inertia_, f)

        with open(full_dir_path + "path_integral_cost_.pkl", 'wb') as f:
            pickle.dump(self.path_integral_cost_, f) 

        with open(full_dir_path + "path_mechanical_work_.pkl", 'wb') as f:
            pickle.dump(self.path_mechanical_work_, f)


    def read_metrics_from_files(self, dir_path, filename):
        full_dir_path = self.base_path + dir_path
        full_path = full_dir_path + filename
        
        #this is a test, implement loading all files back into lists...
        with open(full_path, 'rb') as f:
            data = pickle.load(f)

        print(data)
