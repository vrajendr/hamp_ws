#!/usr/bin/env python

import os
import errno
import pickle
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import copy
import numpy as np

import rospy
import rospkg

from textwrap import fill

# import os
# rospack = rospkg.RosPack()
# os.environ['MPLCONFIGDIR'] = rospack.get_path('hamp_masters_thesis_tests') + "/matplotlib-styles"
# print(rospack.get_path('hamp_masters_thesis_tests') + "/matplotlib-styles/")

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib

# matplotlib.style.reload_library()

plt.style.use(['science']) #for written thesis

#plt.style.use(['seaborn-white']) #for presentations
# plt.style.use(['fivethirtyeight'])

# plt.style.use('fivethirtyeight')
# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')
# plt.rcParams['font.family'] = "serif"
# plt.rcParams['font.serif'] = 'CMU Serif'
# # plt.rcParams['font.monospace'] = 'Computer Modern Typewriter'
# plt.rcParams['font.size'] = 12
# plt.rcParams['axes.labelsize'] = 12
# plt.rcParams['axes.labelweight'] = 'normal'
# plt.rcParams['axes.titlesize'] = 12
# plt.rcParams['xtick.labelsize'] = 10
# plt.rcParams['ytick.labelsize'] = 10
# plt.rcParams['legend.fontsize'] = 12
# plt.rcParams['figure.titlesize'] = 14
# plt.rcParams['figure.titleweight'] = 'bold'

class MetricsPlotter:
    def __init__(self, package_name):

        self.package = package_name
        rospack = rospkg.RosPack()
        self.pkg_dir = rospack.get_path(self.package)
        self.base_path = self.pkg_dir + "/scripts/plots"

    def plot_parameter_sensitivity(self, relative_path, filename, parameter_label, metric_label, metric_data, parameter_sweep, title=None, color = 'r', clear=True):
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        
        #clear the plot before calling function again or else all previous data will show up on the graph...useful sometimes
        if clear:
            plt.clf()
            plt.cla()
            plt.close()

        x = parameter_sweep
        
        avg = []
        std = []
        for i in range(len(metric_data)):
            avg.append(np.mean(metric_data[i]))
            std.append(np.std(metric_data[i])/np.sqrt(len(metric_data[i])))


        self.errorfill(x,np.array(avg), np.array(std), color=color)

        plt.xlabel(parameter_label)
        plt.ylabel(metric_label)

        if title:
            plt.title(title, fontsize='medium')

        #below is for longer legend text
        # legend = [fill(l, 20) for l in legend]
        # plt.legend(legend, loc='best', fontsize=5) 

        plt.tight_layout()
        
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')



    def plot_inertia_com_subplots(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = ['a) Avg. Robot Inertia', 'b) Avg. Human-Robot CoM Distance']
        axes_labels = ['$kg \cdot m^2$','CoM Distance (m)']
        fig, axes = plt.subplots(nrows=2, ncols=1, sharey=False, sharex=False, figsize=(4, 3))
        fig.subplots_adjust(hspace=0.5)

        data_index = 0
        for i in range(2):
            #for j in range(1):

            mean_1 = np.mean(dataset1[data_index])
            std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
            if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                std_1 = mean_1*0.5
            mean_2 = np.mean(dataset2[data_index])
            std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
            if std_2/mean_2 > 0.5:
                std_2 = mean_2*0.5
            
            # Create lists for the plot
            y_pos = np.arange(len(dataset_labels))
            planner_data = [mean_1, mean_2]
            error = [std_1, std_2]

            horizontal_bars = axes[i].barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = ['orange', 'blue'], alpha=0.7)
            
            # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
            # self.label_bars(axes[i,j], horizontal_bars, value_format)

            #Remove y-axis labels and ticks
            axes[i].set_yticks([]) 
            axes[i].set_yticklabels(('',''))
            axes[i].invert_yaxis()  # labels read top-to-bottom
            
            axes[i].set_xlabel(axes_labels[data_index])
            axes[i].set_title(titles[data_index])#, fontweight="bold", size=14)

            plt.setp(axes[i].get_xticklabels(), fontsize='x-small')

            data_index += 1


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.02), fontsize='small')

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.25) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_vis_comparison(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = 'Path Visiblity'
        axes_labels = 'Percent of Path within Gaze Cone'
        fig, axes = plt.subplots(nrows=1, ncols=1, sharey=False, sharex=False, figsize=(4, 3))
        fig.subplots_adjust(hspace=0.5)

        mean_1 = np.mean(dataset1)
        std_1 = np.std(dataset1)/np.sqrt(len(dataset1))

        if mean_1 == 0.0:
            mean_1 = 0.01

        if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
            std_1 = mean_1*0.5
        mean_2 = np.mean(dataset2)

        if mean_2 == 0.0:
            mean_2 = 0.01

        std_2 = np.std(dataset2)/np.sqrt(len(dataset2))
        if std_2/mean_2 > 0.5:
            std_2 = mean_2*0.5
        
        # Create lists for the plot
        y_pos = np.arange(len(dataset_labels))
        planner_data = [mean_1, mean_2]
        error = [std_1, std_2]

        horizontal_bars = axes.barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = colours, alpha=0.7)
        
        # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
        # self.label_bars(axes[i,j], horizontal_bars, value_format)

        #Remove y-axis labels and ticks
        axes.set_yticks([]) 
        axes.set_yticklabels(('',''))
        axes.invert_yaxis()  # labels read top-to-bottom
        
        axes.set_xlabel(axes_labels)
        axes.set_title(titles)#, fontweight="bold", size=14)

        plt.setp(axes.get_xticklabels(), fontsize='x-small')


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.02), fontsize='small')

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.25) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_dist_comparison_2(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = ['a) Min. Separation Distance', 'b) Avg. Separation Distance', 'c) Avg. Path Length']
        axes_labels = ['Separation Distance (m)','Separation Distance (m)', 'Length (m)']
        fig, axes = plt.subplots(nrows=3, ncols=1, sharey=False, sharex=False, figsize=(8, 6))
        fig.subplots_adjust(hspace=0.5)

        data_index = 0
        for i in range(3):
            #for j in range(1):

            mean_1 = np.mean(dataset1[data_index])
            std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
            if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                std_1 = mean_1*0.5
            mean_2 = np.mean(dataset2[data_index])
            std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
            if std_2/mean_2 > 0.5:
                std_2 = mean_2*0.5
            
            # Create lists for the plot
            y_pos = np.arange(len(dataset_labels))
            planner_data = [mean_1, mean_2]
            error = [std_1, std_2]

            horizontal_bars = axes[i].barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = colours, alpha=0.7)
            
            # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
            # self.label_bars(axes[i,j], horizontal_bars, value_format)

            #Remove y-axis labels and ticks
            axes[i].set_yticks([]) 
            axes[i].set_yticklabels(('',''))
            axes[i].invert_yaxis()  # labels read top-to-bottom
            
            axes[i].set_xlabel(axes_labels[data_index])
            axes[i].set_title(titles[data_index])#, fontweight="bold", size=14)

            plt.setp(axes[i].get_xticklabels(), fontsize='x-small')

            data_index += 1


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.01), fontsize='small')

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.125) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_dist_comparison(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = 'Average Separation Distance'
        axes_labels = 'Separation Distance (m)'
        fig, axes = plt.subplots(nrows=1, ncols=1, sharey=False, sharex=False, figsize=(4, 3))
        fig.subplots_adjust(hspace=0.5)

        mean_1 = np.mean(dataset1)
        std_1 = np.std(dataset1)/np.sqrt(len(dataset1))

        if mean_1 == 0.0:
            mean_1 = 0.01

        # this is for the odd chance that the error is higher than 50% so that the graph doesn't look weird
        if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
            std_1 = mean_1*0.5
        mean_2 = np.mean(dataset2)

        if mean_2 == 0.0:
            mean_2 = 0.01

        std_2 = np.std(dataset2)/np.sqrt(len(dataset2))
        if std_2/mean_2 > 0.5:
            std_2 = mean_2*0.5
        
        # Create lists for the plot
        y_pos = np.arange(len(dataset_labels))
        planner_data = [mean_1, mean_2]
        error = [std_1, std_2]

        horizontal_bars = axes.barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = ['orange', 'blue'], alpha=0.7)
        
        # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
        # self.label_bars(axes[i,j], horizontal_bars, value_format)

        #Remove y-axis labels and ticks
        axes.set_yticks([]) 
        axes.set_yticklabels(('',''))
        axes.invert_yaxis()  # labels read top-to-bottom
        
        axes.set_xlabel(axes_labels)
        axes.set_title(titles)#, fontweight="bold", size=14)

        plt.setp(axes.get_xticklabels(), fontsize='x-small')


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.02), fontsize='small')

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.25) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_all_metrics_subplots(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = ['a) Min. Clearance', 'b) Avg. Clearance', 'c) Path Visibility','d) Path Length', 'e) Planning Time', 'f) Number of Nodes', 'g) Robot Inertia', 'h) Integral Cost', 'i) Mechanical Work']
        axes_labels = ['Clearance (m)','Clearance (m)', 'Percent of Path within Gaze Cone','Length (m)', 'Time (s)', '', '$kg \cdot m^2$', '', '']
        fig, axes = plt.subplots(nrows=3, ncols=3, sharey=False, sharex=False, figsize=(10, 6))
        fig.subplots_adjust(hspace=0.5)

        data_index = 0
        for i in range(3):
            for j in range(3):

                mean_1 = np.mean(dataset1[data_index])
                std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
                if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                    std_1 = mean_1*0.5
                mean_2 = np.mean(dataset2[data_index])
                std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
                if std_2/mean_2 > 0.5:
                    std_2 = mean_2*0.5
                
                # Create lists for the plot
                y_pos = np.arange(len(dataset_labels))
                planner_data = [mean_1, mean_2]
                error = [std_1, std_2]

                horizontal_bars = axes[i,j].barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = colours, alpha=0.7)
                
                # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
                # self.label_bars(axes[i,j], horizontal_bars, value_format)

                #Remove y-axis labels and ticks
                axes[i,j].set_yticks([]) 
                axes[i,j].set_yticklabels(('',''))
                axes[i,j].invert_yaxis()  # labels read top-to-bottom
                
                axes[i,j].set_xlabel(axes_labels[data_index])
                axes[i,j].set_title(titles[data_index])#, fontweight="bold", size=14)

                plt.setp(axes[i,j].get_xticklabels(), fontsize='x-small')

                data_index += 1


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.02))

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.125) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_all_metrics_subplots_paper(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        titles = ['a) Min. Clearance', 'b) Avg. Clearance', 'c) Path Visibility','d) Path Length', 'e) Planning Time', 'f) Number of Nodes', 'g) Robot Inertia', 'h) Integral Cost', 'i) Mechanical Work']
        axes_labels = ['Clearance (m)','Clearance (m)', 'Percent of Path within Gaze Cone','Length (m)', 'Time (s)', '', '$kg \cdot m^2$', '', '']
        fig, axes = plt.subplots(nrows=3, ncols=3, sharey=False, sharex=False, figsize=(7, 3))
        fig.subplots_adjust(hspace=0.5)

        data_index = 0
        for i in range(3):
            for j in range(3):

                mean_1 = np.mean(dataset1[data_index])
                std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
                if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                    std_1 = mean_1*0.5
                mean_2 = np.mean(dataset2[data_index])
                std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
                if std_2/mean_2 > 0.5:
                    std_2 = mean_2*0.5
                
                # Create lists for the plot
                y_pos = np.arange(len(dataset_labels))
                planner_data = [mean_1, mean_2]
                error = [std_1, std_2]

                horizontal_bars = axes[i,j].barh(y_pos, planner_data, xerr=error, align='center', ecolor='black', color = colours, alpha=0.7)
                
                # value_format = "{:.2f}%"  # displaying values as percentage with one fractional digit
                # self.label_bars(axes[i,j], horizontal_bars, value_format)

                #Remove y-axis labels and ticks
                axes[i,j].set_yticks([]) 
                axes[i,j].set_yticklabels(('',''))
                axes[i,j].invert_yaxis()  # labels read top-to-bottom
                
                axes[i,j].set_xlabel(axes_labels[data_index], fontsize='x-small')
                axes[i,j].set_title(titles[data_index], fontsize='x-small')

                plt.setp(axes[i,j].get_xticklabels(), fontsize='xx-small')

                data_index += 1


        legend_labels = dataset_labels
        # now, create an artist for each color
        label1 = mpatches.Patch(facecolor=colours[0], edgecolor='#000000', alpha=0.7)
        label2 = mpatches.Patch(facecolor=colours[1], edgecolor='#000000', alpha=0.7)
        #Add legend to figure and place it at the bottom
        fig.legend(handles = [label1, label2],labels=legend_labels, 
            loc="lower center", 
            borderaxespad=0.1, bbox_to_anchor=(0.5, -0.02), fontsize='x-small')

        plt.tight_layout()
        plt.subplots_adjust(bottom=0.125) #add a little more space at the bottom separating the legend from the plots
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_all_metrics_subplots_paper_v2(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        SMALL_SIZE = 8
        MEDIUM_SIZE = 10
        BIGGER_SIZE = 12

        plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
        plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
        plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize

        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        
        labels = ['a) Min. Clearance', 'b) Avg. Clearance', 'c) Path Visibility','d) Path Length', 'e) Planning Time', 'f) Number of Nodes', 'g) Robot Inertia', 'h) Integral Cost', 'i) Mechanical Work']
        #labels = ['a)', 'b)', 'c)','d)', 'e)', 'f)', 'g)', 'h)', 'i)']
        # labels = ['a)', 'b)', 'c)','d)']
        #axes_labels = ['Clearance (m)','Clearance (m)', 'Percent of Path within Gaze Cone','Length (m)', 'Time (s)', '', '$kg \cdot m^2$', '', '']
        
        planner_1 = []
        planner_1_error = []
        planner_2 = []
        planner_2_error = []
        data_index = 0
        for i in range(9):
            # if i == 5:
            #     continue

            mean_1 = np.mean(dataset1[data_index])
            std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
            if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                std_1 = mean_1*0.5
            mean_2 = np.mean(dataset2[data_index])
            std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
            if std_2/mean_2 > 0.5:
                std_2 = mean_2*0.5
            
            mean_scaling = max(mean_1, mean_2)
            std_scaling = mean_scaling
            
            if i == 4 or i == 5 or i == 6 or i == 7 or i == 8:
                mean_1 = mean_1/mean_scaling
                mean_2 = mean_2/mean_scaling

                std_1 = std_1/std_scaling
                std_2 = std_2/std_scaling

            planner_1.append(mean_1) 
            planner_2.append(mean_2)

            planner_1_error.append(std_1) 
            planner_2_error.append(std_2)

            data_index += 1

        x = np.arange(len(labels))  # the label locations
        width = 0.4  # the width of the bars

        bar_edge_color = ['black', 'black' , 'black' , 'black', 'r', 'r', 'r', 'r' , 'r']
        fig, ax = plt.subplots(figsize=(4, 3))
        rects1 = ax.barh(x - width/2, planner_1, width, xerr = planner_1_error, 
            error_kw=dict(ecolor='black', capsize=0), label=dataset_labels[0], color=colours[0], align="center",
            edgecolor=bar_edge_color)
        rects2 = ax.barh(x + width/2, planner_2, width, xerr = planner_2_error,
            error_kw=dict(ecolor='black', capsize=0), label=dataset_labels[1], color=colours[1], align="center",
            edgecolor=bar_edge_color)

        c = rects1.errorbar.lines[2][rects1.errorbar.has_yerr] # <----
        c.set_color(bar_edge_color)

        c = rects2.errorbar.lines[2][rects2.errorbar.has_yerr] # <----
        c.set_color(bar_edge_color)

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_xlabel('Metric Values')
        #ax.set_title('Scores by group and gender')
        ax.set_yticks(x)
        ax.set_yticklabels(labels, ha='right', rotation=0)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.legend()

        def autolabel(rects, planner_error):
            """Attach a text label above each bar in *rects*, displaying its height."""
            i = 0
            for rect in rects:


                width = rect.get_width()

                if width <= 1.01 and width >= 0.99:
                    i+= 1
                    continue

                ax.annotate('{:.2f}'.format(width),
                            xy=(width + planner_error[i], rect.get_y() + rect.get_height()*0.575),#+ max(planner_1_error[i], planner_2_error[i])),
                            xytext=(8, 0.0), 
                            textcoords="offset points",
                            ha='center', va='center', fontsize=6, rotation=0)
                i+= 1
                

        autolabel(rects1, planner_1_error)
        autolabel(rects2, planner_2_error)

        plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=True,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off

        plt.tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        left=False,      # ticks along the bottom edge are off
        right=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off


        plt.legend(loc="best", prop={'size': 6})
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)

        fig.tight_layout()
        
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_all_metrics_subplots_paper_v1(self, relative_path, filename, dataset_labels, dataset1, dataset2, colours=['orange', 'blue']):
        
        SMALL_SIZE = 8
        MEDIUM_SIZE = 10
        BIGGER_SIZE = 12

        plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
        plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
        plt.rc('axes', labelsize=SMALL_SIZE)    # fontsize of the x and y labels
        plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
        plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize

        # make sure dataset_labels is a list e.g. ['planner1', 'planner2'] and dataset1 and dataset2 correspond correctly to the labels
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        
        labels = ['a) Min. Clearance', 'b) Avg. Clearance', 'c) Path Visibility','d) Path Length', 'e) Planning Time', 'f) Number of Nodes', 'g) Robot Inertia', 'h) Integral Cost', 'i) Mechanical Work']
        #labels = ['a)', 'b)', 'c)','d)', 'e)', 'f)', 'g)', 'h)', 'i)']
        # labels = ['a)', 'b)', 'c)','d)']
        #axes_labels = ['Clearance (m)','Clearance (m)', 'Percent of Path within Gaze Cone','Length (m)', 'Time (s)', '', '$kg \cdot m^2$', '', '']
        
        planner_1 = []
        planner_1_error = []
        planner_2 = []
        planner_2_error = []
        data_index = 0
        for i in range(9):
            # if i == 5:
            #     continue

            mean_1 = np.mean(dataset1[data_index])
            std_1 = np.std(dataset1[data_index])/np.sqrt(len(dataset1[data_index]))
            if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
                std_1 = mean_1*0.5
            mean_2 = np.mean(dataset2[data_index])
            std_2 = np.std(dataset2[data_index])/np.sqrt(len(dataset2[data_index]))
            if std_2/mean_2 > 0.5:
                std_2 = mean_2*0.5
            
            mean_scaling = max(mean_1, mean_2)
            std_scaling = mean_scaling
            
            if i == 4 or i == 5 or i == 6 or i == 7 or i == 8:
                mean_1 = mean_1/mean_scaling
                mean_2 = mean_2/mean_scaling

                std_1 = std_1/std_scaling
                std_2 = std_2/std_scaling

            planner_1.append(mean_1) 
            planner_2.append(mean_2)

            planner_1_error.append(std_1) 
            planner_2_error.append(std_2)

            data_index += 1

        x = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars

        bar_edge_color = ['black', 'black' , 'black' , 'black', 'r', 'r', 'r', 'r' , 'r']
        fig, ax = plt.subplots()
        rects1 = ax.bar(x - width/2, planner_1, width, yerr = planner_1_error, 
            error_kw=dict(ecolor='black', capsize=0), label=dataset_labels[0], color=colours[0], align="center",
            edgecolor=bar_edge_color)
        rects2 = ax.bar(x + width/2, planner_2, width, yerr = planner_2_error,
            error_kw=dict(ecolor='black', capsize=0), label=dataset_labels[1], color=colours[1], align="center",
            edgecolor=bar_edge_color)

        c = rects1.errorbar.lines[2][rects1.errorbar.has_xerr] # <----
        c.set_color(bar_edge_color)

        c = rects2.errorbar.lines[2][rects2.errorbar.has_xerr] # <----
        c.set_color(bar_edge_color)

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Metric Values')
        #ax.set_title('Scores by group and gender')
        ax.set_xticks(x)
        ax.set_xticklabels(labels, ha='right', rotation=45)
        ax.legend()

        def autolabel(rects, planner_error):
            """Attach a text label above each bar in *rects*, displaying its height."""
            i = 0
            for rect in rects:


                height = rect.get_height()

                if height <= 1.01 and height >= 0.99:
                    i+= 1
                    continue

                ax.annotate('{:.2f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height + planner_error[i]),#+ max(planner_1_error[i], planner_2_error[i])),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom', fontsize=5, rotation=90)
                i+= 1
                

        autolabel(rects1, planner_1_error)
        autolabel(rects2, planner_2_error)

        plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off

        plt.tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        left=True,      # ticks along the bottom edge are off
        right=False,         # ticks along the top edge are off
        labelbottom=True) # labels along the bottom edge are off


        plt.legend(loc="best", prop={'size': 5.5})
        plt.xticks(fontsize=6)
        plt.yticks(fontsize=6)

        fig.tight_layout()
        
        #plt.show()
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

    def plot_two_dataset_avg_errs(self, dataset1, dataset2, dataset1_name, dataset2_name, ylabel, title, relative_path, filename):
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        mean_1 = np.mean(dataset1)
        std_1 = np.std(dataset1)/np.sqrt(len(dataset1))
        if std_1/mean_1 > 0.5: #bound the error here and note it in the thesis...
            std_1 = mean_1*0.5

        mean_2 = np.mean(dataset2)
        std_2 = np.std(dataset2)/np.sqrt(len(dataset2))
        if std_2/mean_2 > 0.5:
            std_2 = mean_2*0.5

        # Create lists for the plot
        dataset_names = [dataset1_name, dataset2_name]
        x_pos = np.arange(len(dataset_names))
        planner_data = [mean_1, mean_2]
        error = [std_1, std_2]

        # Build the plot
        _, ax = plt.subplots()
        ax.bar(x_pos, planner_data, yerr=error, align='center', alpha=0.6, ecolor='black', color = ['orange', 'blue'], capsize=10)
        ax.set_ylabel(ylabel)
        ax.set_xticks(x_pos)
        ax.set_xticklabels(dataset_names)
        ax.set_title(title)
        ax.yaxis.grid(True)
        plt.tight_layout()
        plt.savefig(full_dir_path + filename, dpi=300)
        #plt.show()

    def plot_two_series(self, label1, data1, label2, data2, xlabel, ylabel, title, relative_path):

        plt.rc('xtick', labelsize='x-small')
        plt.rc('ytick', labelsize='x-small')
        fig = plt.figure(figsize=(8, 6))
        ax = fig.add_subplot(1, 1, 1)
        ax.plot(data1, linewidth=3.0, color='0.50', linestyle='-', label=label1)
        ax.plot(data2, linewidth=3.0, color='0.50', linestyle='--', label=label2)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        plt.title(title)
        plt.legend(loc='best')
        plt.savefig(self.base_path + relative_path, dpi=300)
        #plt.show()

    def get_col(self, arr, col):
        return map(lambda x : x[col], arr)

    def plot_full_trajectory_metric(self, data, legend, relative_path, filename, labelx='', labely='', title=None, colors=['b', 'r'], clear=False):
        
        full_dir_path = self.base_path + relative_path
        if not os.path.exists(full_dir_path):
            try:
                os.makedirs(full_dir_path)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        
        #clear the plot before calling function again or else all previous data will show up on the graph...useful sometimes
        if clear:
            plt.clf()
            plt.cla()
            plt.close()

        data_row_length = len(data[0][0])
        # for i in range(0, len(data)):
        #     row_length = len(data[i])
        #     if row_length == data_row_length:
        #         continue
        #     else:
        #         raise Exception("Trajectories do not have the same number of waypoints.")

        #make a new list with the averaged data and another with the standard deviation

        #mean and std of each column upto min_row_length
        for k in range(len(data)):
            avg = []
            std = []
            for j in range(0,data_row_length):
                # print(j)
                column = np.array(self.get_col(data[k],j))
                avg.append(np.mean(column))
                std.append(np.std(column)/np.sqrt(len(column)))

            x = np.linspace(0, len(avg)-1, len(avg), endpoint=True)

            if k == len(data)-1:
                self.errorfill(x,np.array(avg), np.array(std), color=colors[k], ax=plt.gca())
            else:
                self.errorfill(x,np.array(avg), np.array(std), color=colors[k])
        
        plt.xlabel(labelx)
        plt.ylabel(labely)
        if title:
            plt.title(title, fontsize='medium')

        #below is for the cone of gaze region for the visibility plot
        plt.axhspan(0, np.deg2rad(15), color='green', alpha=0.2)

        #below is for longer legend text
        # legend = [fill(l, 20) for l in legend]
        # plt.legend(legend, loc='best', fontsize=5) 

        plt.legend(legend, loc='best', fontsize='x-small') 
        plt.tight_layout()
        
        plt.savefig(full_dir_path + filename, dpi=300, bbox_inches='tight')

        #plt.show()

    def errorfill(self, x, y, yerr, color=None, alpha_fill=0.3, ax=None):
        if ax == None:
            fig = plt.figure(figsize=(4, 3))
            ax = fig.add_subplot(1, 1, 1)

            #below needed for visibility plot
            # arrowprops={'arrowstyle': '-|>', 'lw': 1, 'ec': 'black'}
            # ax.annotate('Cone of gaze region', xy=(80-5, 0.26), xytext=(62-5, 0.4),
            #             arrowprops=arrowprops, fontsize=9
            #             )

        # ax = ax if ax is not None else plt.gca()
        
        if color is None:
            color = 'b'
        if np.isscalar(yerr) or len(yerr) == len(y):
            ymin = y - yerr
            ymax = y + yerr
        elif len(yerr) == 2:
            ymin, ymax = yerr
        ax.plot(x, y, color=color, linewidth="2")
        ax.fill_between(x, ymax, ymin, color=color, alpha=alpha_fill, lw="1")
        plt.setp(ax.get_xticklabels(), fontsize='small')
        plt.setp(ax.get_yticklabels(), fontsize='small')

    #https://stackoverflow.com/questions/30228069/how-to-display-the-value-of-the-bar-on-each-bar-with-pyplot-barh
    def label_bars(self, ax, bars, text_format, **kwargs):
        """
        Attaches a label on every bar of a regular or horizontal bar chart
        """
        ys = [bar.get_y() for bar in bars]
        y_is_constant = all(y == ys[0] for y in ys)  # -> regular bar chart, since all all bars start on the same y level (0)

        if y_is_constant:
            self._label_bar(ax, bars, text_format, **kwargs)
        else:
            self._label_barh(ax, bars, text_format, **kwargs)


    def _label_bar(self, ax, bars, text_format, **kwargs):
        """
        Attach a text label to each bar displaying its y value
        """
        max_y_value = ax.get_ylim()[1]
        inside_distance = max_y_value * 0.05
        outside_distance = max_y_value * 0.01

        for bar in bars:
            text = text_format.format(bar.get_height())
            text_x = bar.get_x() + bar.get_width() / 2

            is_inside = bar.get_height() >= max_y_value * 0.15
            if is_inside:
                color = "white"
                text_y = bar.get_height() - inside_distance
            else:
                color = "black"
                text_y = bar.get_height() + outside_distance

            ax.text(text_x, text_y, text, ha='center', va='bottom', color=color, **kwargs)


    def _label_barh(self, ax, bars, text_format, **kwargs):
        """
        Attach a text label to each bar displaying its y value
        Note: label always outside. otherwise it's too hard to control as numbers can be very long
        """
        max_x_value = ax.get_xlim()[1]
        distance = max_x_value * 0.0025

        for bar in bars:
            text = text_format.format(bar.get_width())

            text_x = bar.get_width() + distance
            text_y = bar.get_y() + bar.get_height() / 2

            ax.text(text_x, text_y, text, va='bottom', **kwargs)