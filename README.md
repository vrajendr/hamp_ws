# hamp_ws

Dockerfile and ROS workspace for Human Aware Motion Planning

1. `git submodule update --init --recursive`
2. `roslaunch hamp_ros panda_planning_testing_pipeline.launch` or `roslaunch human_aware_planning panda_prm_demo.launch`
3. `rosrun human_aware_planning com_publisher.py`
4. `rosrun human_model human_model_data_srv` (this spawns a service that returns, for a specific configuration, distance to human at each robot link, visibility angle at each robot link, collision check)
5. Run any testing scripts etc.