SHELL := /bin/bash

base:
	docker build -t hamp_latest -t hamp_base -f docker/DockerfileBase .

nvidia:
	docker build -t hamp_latest -t hamp_nvidia -f docker/DockerfileNvidia --build-arg BASE_IMAGE=hamp_base .
